import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { configureActions } from '@storybook/addon-actions';
import 'react-tippy/dist/tippy.css';
import '../src/index.css';

import '@fortawesome/fontawesome-pro/css/all.css';
import '@fortawesome/fontawesome-pro/js/all.js';

function loadStories() {
    require('../stories/index.tsx');
}

addDecorator(story => (
    <div className="container-fluid">
        {story()}
    </div>
));

configure(loadStories, module);
