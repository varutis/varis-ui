# Development

To start develpment server execute `npm start` and open url http://localhost:9001 in your browser.

```
npm run start
```

# Release

Build and commit:
```
npm run build
git commit -am "Message..."
```

Pull and push changes

Execute one of the following commands:

```
npm run release:patch
npm run release:minor
npm run release:major
```

Package will be built, labeled and pushed to the git remote with version tags.