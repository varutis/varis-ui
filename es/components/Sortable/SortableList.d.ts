/// <reference types="react" />
import * as React from 'react';
import { SortableContainerProps } from 'react-sortable-hoc';
export interface SortableListProps {
}
export declare const SortableList: React.ComponentClass<SortableContainerProps & SortableListProps>;
