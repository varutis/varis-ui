var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { SortableContainer } from 'react-sortable-hoc';
var SortableListComponent = /** @class */ (function (_super) {
    __extends(SortableListComponent, _super);
    function SortableListComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SortableListComponent.prototype.render = function () {
        var children = this.props.children;
        return (React.createElement("div", null, children));
    };
    return SortableListComponent;
}(React.Component));
export var SortableList = SortableContainer(SortableListComponent);
//# sourceMappingURL=SortableList.js.map