/// <reference types="react" />
import * as React from 'react';
import { SortableElementProps } from 'react-sortable-hoc';
export interface SortableListItemProps {
}
export declare const SortableListItem: React.ComponentClass<SortableElementProps & SortableListItemProps>;
