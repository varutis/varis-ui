/// <reference types="react" />
import * as React from 'react';
import { FieldProps } from '.';
export declare type CheckBoxInputProps = {
    name: string;
    className?: string;
    description?: string;
    form?: string;
    text?: string;
    value?: boolean;
    disabled?: boolean;
    autoFocus?: boolean;
    autocomplete?: string;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
    style?: React.CSSProperties;
};
export declare class CheckBoxInput extends React.Component<CheckBoxInputProps> {
    static parse: (value: string | boolean) => boolean;
    render(): JSX.Element;
}
export default CheckBoxInput;
export declare const CheckBoxField: React.ComponentClass<FieldProps & CheckBoxInputProps>;
