var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import * as cn from 'classnames';
import { Tooltip } from 'react-tippy';
import formField from './formField';
import { nameToId } from '../utils';
var CheckBoxInput = /** @class */ (function (_super) {
    __extends(CheckBoxInput, _super);
    function CheckBoxInput() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CheckBoxInput.prototype.render = function () {
        var _a = this.props, autoFocus = _a.autoFocus, className = _a.className, description = _a.description, disabled = _a.disabled, name = _a.name, form = _a.form, text = _a.text, value = _a.value, onBlur = _a.onBlur, onChange = _a.onChange, onFocus = _a.onFocus, style = _a.style, autocomplete = _a.autocomplete;
        return (React.createElement("div", { className: "d-flex flex-row" },
            React.createElement("div", { className: cn('form-check', className), style: style != null ? style : {} },
                React.createElement("label", { className: "form-check-label" },
                    React.createElement("input", { id: name, type: "checkbox", className: "form-check-input", disabled: disabled, autoFocus: autoFocus, checked: !!value, autoComplete: autocomplete, onBlur: onBlur, onChange: onChange, onFocus: onFocus }),
                    text && " " + text,
                    description && React.createElement(Tooltip, { title: description, position: "top", duration: 0, delay: [100, 0], arrow: true, key: "tooltip_" + nameToId(name) },
                        React.createElement("i", { key: "description", id: "" + form + (form && '_') + nameToId(name) + "_descr", className: "ml-2 fas fa-info-circle" }))))));
    };
    CheckBoxInput.parse = function (value) { return !!value; };
    return CheckBoxInput;
}(React.Component));
export { CheckBoxInput };
export default CheckBoxInput;
export var CheckBoxField = formField(CheckBoxInput);
//# sourceMappingURL=CheckBoxInput.js.map