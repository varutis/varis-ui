/// <reference types="react" />
import * as React from 'react';
import { Moment } from 'moment';
import 'react-datetime/css/react-datetime.css';
import 'moment/locale/lt';
import { FieldProps } from '.';
export interface DateTimeInputProps {
    autoFocus?: boolean;
    className?: string;
    description?: string;
    disabled?: boolean;
    form?: string;
    name: string;
    placeholder?: string;
    showDate?: boolean | string;
    showTime?: boolean | string;
    showToggleButton?: boolean;
    value?: Moment;
    autoComplete?: string;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
}
export interface DateTimeInputState {
    isOpen: boolean;
}
export declare class DateTimeInput extends React.Component<DateTimeInputProps, DateTimeInputState> {
    private component;
    constructor(props: DateTimeInputProps);
    handleToggle: (e: React.MouseEvent<{}>) => void;
    handleOnChange: (e: any) => void;
    handleOnBlur: (e: any) => void;
    handleOnFocus: (e: any, showToggleButton: boolean) => void;
    handleOnClick: (e: any, showToggleButton: boolean) => void;
    toggleButton: () => JSX.Element;
    render(): JSX.Element;
}
export default DateTimeInput;
export declare const DateTimeField: React.ComponentClass<FieldProps & DateTimeInputProps>;
