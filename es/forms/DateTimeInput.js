var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import * as React from 'react';
import * as ReactDatetime from 'react-datetime';
import { findDOMNode } from 'react-dom';
import formField from './formField';
import Input from './Input';
import 'react-datetime/css/react-datetime.css';
import 'moment/locale/lt';
var DateTimeInput = /** @class */ (function (_super) {
    __extends(DateTimeInput, _super);
    function DateTimeInput(props) {
        var _this = _super.call(this, props) || this;
        _this.handleToggle = function (e) {
            e.preventDefault();
            if (_this.component) {
                var input = findDOMNode(_this.component).querySelector('input');
                if (input) {
                    input.focus();
                    var isOpen = _this.state.isOpen;
                    _this.setState({ isOpen: !isOpen });
                }
            }
        };
        _this.handleOnChange = function (e) {
            if (_this.props.onChange) {
                _this.props.onChange(e);
                _this.setState({ isOpen: false });
            }
        };
        _this.handleOnBlur = function (e) {
            if (_this.props.onBlur) {
                _this.props.onBlur(e);
                _this.setState({ isOpen: false });
            }
        };
        _this.handleOnFocus = function (e, showToggleButton) {
            if (_this.props.onFocus) {
                _this.props.onFocus(e);
                if (showToggleButton) {
                    _this.setState({ isOpen: true });
                }
                else {
                    _this.setState({ isOpen: false });
                }
            }
        };
        _this.handleOnClick = function (e, showToggleButton) {
            if (!showToggleButton) {
                _this.setState({ isOpen: false });
            }
        };
        _this.toggleButton = function () {
            return (React.createElement("div", { key: "toggle", className: "input-group-append" },
                React.createElement("button", { className: "btn btn-outline-secondary", type: "button", onClick: _this.handleToggle },
                    React.createElement("i", { className: "fas fa-calendar" }))));
        };
        _this.state = {
            isOpen: false
        };
        return _this;
    }
    DateTimeInput.prototype.render = function () {
        var _this = this;
        var props = __assign({ showDate: true, showTime: false, showToggleButton: true }, this.props);
        var dateTimeProps = {
            inputProps: {
                autoFocus: props.autoFocus,
                className: 'form-control' + (props.className ? " " + props.className : ''),
                disabled: props.disabled,
                name: props.name,
                placeholder: props.placeholder,
                autoComplete: props.autoComplete,
                onClick: function (e) { _this.handleOnClick(e, props.showToggleButton); },
                onFocus: function (e) { _this.handleOnFocus(e, props.showToggleButton); } // tslint:disable-line no-any
            },
            open: this.state.isOpen,
            dateFormat: props.showDate,
            timeFormat: props.showTime,
            locale: 'lt',
            strictParsing: true,
            utc: false,
            value: props.value,
            onBlur: function (e) { _this.handleOnBlur(e); },
            onChange: function (e) { _this.handleOnChange(e); },
            onFocus: function (e) { _this.handleOnFocus(e, props.showToggleButton); } // tslint:disable-line no-any
        };
        return (React.createElement(Input, { name: props.name, form: props.form, component: (React.createElement(ReactDatetime, __assign({ key: "component", ref: function (x) { return _this.component = x; } }, dateTimeProps))), addons: props.showToggleButton ? [this.toggleButton()] : undefined, description: props.description }));
    };
    return DateTimeInput;
}(React.Component));
export { DateTimeInput };
export default DateTimeInput;
export var DateTimeField = formField(DateTimeInput);
//# sourceMappingURL=DateTimeInput.js.map