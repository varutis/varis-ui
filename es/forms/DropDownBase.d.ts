/// <reference types="react" />
import * as React from 'react';
import { Option } from '../model';
export interface DropDownItem extends Option<any> {
}
export declare const messages: {
    loading: string;
    noItems: string;
};
export interface DropDownProps {
    name: string;
    form?: string;
    className?: string;
    disabled?: boolean;
    description?: string;
    placeholder?: string;
    addons?: React.ReactElement<{}>[];
    size?: 'sm' | 'lg';
    iconName: string;
    emptyMessage?: string;
    minSearchSymbols?: number;
    multiSection?: boolean;
    showGroupTitle?: boolean;
    focusOnItemClick?: boolean;
    loading?: boolean;
    items: DropDownItem[];
    showCodeWithName?: boolean;
    value: DropDownItem | null;
    onFocus?: React.FocusEventHandler<{}>;
    onBlur?: (newValue: DropDownItem | null) => void;
    onChange: (newValue: DropDownItem | null) => void;
    onOpen?: (text: string) => void;
    onSearch?: (text: string) => void;
}
export interface DropDownState {
    open: boolean;
    selectedIndex: number;
    text: string;
}
export declare class DropDownBase extends React.Component<DropDownProps, DropDownState> {
    static defaultProps: Partial<DropDownProps>;
    private itemJustSelected;
    private selectedValue;
    private inputRef;
    constructor(props: DropDownProps);
    componentWillReceiveProps(nextProps: DropDownProps): void;
    open(): void;
    search(text: string): void;
    close(): void;
    focus(): void;
    getMinSearchSymbols(): number;
    handleFocus: (e: React.FocusEvent<{}>) => void;
    handleBlur: () => void;
    handleClick: () => void;
    handleKeyDown: (e: React.KeyboardEvent<{}>) => void;
    handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleItemClick: (e: React.MouseEvent<{}>, item: DropDownItem) => void;
    handleIconClick: (e: React.MouseEvent<{}>) => void;
    processArrowDown(e: React.KeyboardEvent<{}>): void;
    processArrowUp(e: React.KeyboardEvent<{}>): void;
    processEnter(e: React.KeyboardEvent<{}>): void;
    processEscape(e: React.KeyboardEvent<{}>): void;
    render(): JSX.Element;
    renderInputGroup(): JSX.Element;
    renderInput(): JSX.Element;
    renderIcon(): JSX.Element;
    menuContent: () => JSX.Element;
    renderMenu(): JSX.Element;
    renderMenuItems(): React.ReactNode[];
    renderMenuItem(item: DropDownItem, index: number, showCodeWithName: boolean): JSX.Element;
}
