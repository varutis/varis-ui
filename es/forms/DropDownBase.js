var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import * as cn from 'classnames';
import Input from './Input';
import { nameToId } from '../utils';
export var messages = {
    loading: 'Siunčiami duomenys...',
    noItems: 'Įrašų nerasta'
};
var DropDownBase = /** @class */ (function (_super) {
    __extends(DropDownBase, _super);
    function DropDownBase(props) {
        var _this = _super.call(this, props) || this;
        _this.itemJustSelected = false;
        _this.handleFocus = function (e) {
            if (_this.getMinSearchSymbols() === 0 && _this.props.onSearch) {
                _this.props.onSearch('');
            }
            if (!_this.itemJustSelected) {
                _this.open();
            }
            _this.itemJustSelected = false;
            if (_this.props.onFocus) {
                _this.props.onFocus(e);
            }
        };
        _this.handleBlur = function () {
            _this.close();
            if (_this.props.onBlur) {
                _this.props.onBlur(_this.selectedValue);
            }
        };
        _this.handleClick = function () {
            _this.open();
        };
        _this.handleKeyDown = function (e) {
            if (e.key === 'ArrowDown') {
                _this.processArrowDown(e);
            }
            else if (e.key === 'ArrowUp') {
                _this.processArrowUp(e);
            }
            else if (e.key === 'Enter') {
                _this.processEnter(e);
            }
            else if (e.key === 'Escape') {
                _this.processEscape(e);
            }
        };
        _this.handleChange = function (e) {
            if (_this.selectedValue != null) {
                _this.selectedValue = null;
                _this.props.onChange(_this.selectedValue);
            }
            _this.search(e.target.value);
        };
        _this.handleItemClick = function (e, item) {
            if (item !== _this.selectedValue) {
                _this.props.onChange(item);
            }
            _this.close();
            if (_this.props.focusOnItemClick) {
                _this.itemJustSelected = true;
                _this.focus();
            }
        };
        _this.handleIconClick = function (e) {
            _this.focus();
        };
        _this.menuContent = function () {
            var _a = _this.props, items = _a.items, loading = _a.loading, emptyMessage = _a.emptyMessage, value = _a.value;
            var text = _this.state.text;
            return (React.createElement("span", null,
                _this.renderMenuItems(),
                items.length === 0 && React.createElement("div", { className: "dropdown-item disabled" }, loading ? messages.loading :
                    text === null || text.length < _this.getMinSearchSymbols() || value != null ? emptyMessage : messages.noItems)));
        };
        _this.selectedValue = _this.props.value;
        _this.state = {
            open: false,
            selectedIndex: -1,
            text: _this.selectedValue ?
                _this.props.showCodeWithName === true ?
                    _this.selectedValue.code != null ?
                        _this.selectedValue.code
                        : ''
                    : _this.selectedValue.name
                : ''
        };
        return _this;
    }
    DropDownBase.prototype.componentWillReceiveProps = function (nextProps) {
        if (nextProps.value !== this.selectedValue) {
            var textCode = nextProps.value ? nextProps.value.code : '';
            var textName = nextProps.value ? nextProps.value.name : '';
            var text = nextProps.showCodeWithName === true ? (textCode != null ? textCode : '') : textName;
            this.selectedValue = nextProps.value;
            this.setState({ text: text });
        }
    };
    DropDownBase.prototype.open = function () {
        if (this.props.disabled) {
            return;
        }
        if (this.props.onOpen) {
            this.props.onOpen(this.state.text);
        }
        this.setState({ open: true, selectedIndex: -1 });
    };
    DropDownBase.prototype.search = function (text) {
        if (this.props.disabled) {
            return;
        }
        if (text !== this.state.text) {
            this.setState({ text: text });
        }
        if (this.props.onSearch) {
            this.props.onSearch(text);
        }
        this.setState({ open: true, selectedIndex: -1 });
    };
    DropDownBase.prototype.close = function () {
        var text = this.selectedValue != null ? this.selectedValue.name : '';
        var textCode = this.selectedValue != null ? this.selectedValue.code != null ? this.selectedValue.code : '' : '';
        this.setState({ open: false, text: this.props.showCodeWithName === true ? textCode : text });
    };
    DropDownBase.prototype.focus = function () {
        if (this.inputRef) {
            var inputRef_1 = this.inputRef;
            setTimeout(function () { inputRef_1.focus(); inputRef_1.select(); }, 0);
        }
    };
    DropDownBase.prototype.getMinSearchSymbols = function () {
        return this.props.minSearchSymbols !== undefined ? this.props.minSearchSymbols : 3;
    };
    DropDownBase.prototype.processArrowDown = function (e) {
        e.preventDefault();
        var _a = this.state, open = _a.open, selectedIndex = _a.selectedIndex;
        if (!open) {
            this.open();
        }
        else if (selectedIndex < this.props.items.length - 1) {
            this.setState({ selectedIndex: selectedIndex + 1 });
        }
    };
    DropDownBase.prototype.processArrowUp = function (e) {
        e.preventDefault();
        var _a = this.state, open = _a.open, selectedIndex = _a.selectedIndex;
        if (open && selectedIndex > 0) {
            this.setState({ selectedIndex: selectedIndex - 1 });
        }
    };
    DropDownBase.prototype.processEnter = function (e) {
        if (this.state.open) {
            e.preventDefault();
            var items = this.props.items;
            var selectedIndex = this.state.selectedIndex;
            if (selectedIndex >= 0 && selectedIndex < items.length) {
                var value = items[selectedIndex];
                if (value !== this.selectedValue) {
                    this.props.onChange(value);
                }
            }
            this.close();
        }
    };
    DropDownBase.prototype.processEscape = function (e) {
        e.preventDefault();
        if (this.state.open) {
            this.close();
        }
    };
    DropDownBase.prototype.render = function () {
        return (React.createElement("div", { className: "dropdown" },
            this.renderInputGroup(),
            this.renderIcon(),
            this.renderMenu()));
    };
    DropDownBase.prototype.renderInputGroup = function () {
        return (React.createElement(Input, { name: this.props.name, form: this.props.form, description: this.props.description, component: this.renderInput(), addons: this.props.addons, size: this.props.size }));
    };
    DropDownBase.prototype.renderInput = function () {
        var _this = this;
        var _a = this.props, name = _a.name, form = _a.form, className = _a.className, disabled = _a.disabled, placeholder = _a.placeholder, description = _a.description, addons = _a.addons, size = _a.size;
        var text = this.state.text;
        var hasInputGroup = description || (addons && addons.length > 0);
        var inputSize = !hasInputGroup && (size === 'sm' ? 'form-control-sm' : size === 'lg' ? 'form-control-lg' : undefined);
        return (React.createElement("input", { id: nameToId(name, form), key: "component", ref: function (x) { return _this.inputRef = x; }, type: "text", className: cn('form-control form-control-with-icon', className, inputSize), disabled: disabled, placeholder: placeholder, autoComplete: "off", value: text, onFocus: this.handleFocus, onBlur: this.handleBlur, onClick: this.handleClick, onKeyDown: this.handleKeyDown, onChange: this.handleChange }));
    };
    DropDownBase.prototype.renderIcon = function () {
        var _a = this.props, loading = _a.loading, iconName = _a.iconName;
        return (React.createElement("div", { className: "form-control-icon", onClick: this.handleIconClick },
            React.createElement("i", { className: cn('fas fa-fw', loading ? 'fa-spinner fa-pulse' : iconName) })));
    };
    DropDownBase.prototype.renderMenu = function () {
        var open = this.state.open;
        var items = this.props.items;
        if (items.length > 15) {
            return (React.createElement("div", { className: cn('dropdown-menu', { show: open }), style: { overflowY: 'scroll', height: 410 } }, this.menuContent()));
        }
        return (React.createElement("div", { className: cn('dropdown-menu', { show: open }) }, this.menuContent()));
    };
    DropDownBase.prototype.renderMenuItems = function () {
        var _this = this;
        var _a = this.props, items = _a.items, multiSection = _a.multiSection, showGroupTitle = _a.showGroupTitle, showCodeWithName = _a.showCodeWithName;
        if (items.length === 0) {
            return [];
        }
        if (!multiSection) {
            return items.map(function (item, index) { return _this.renderMenuItem(item, index, showCodeWithName != null ? showCodeWithName : false); });
        }
        var currentGroup = '$$initial';
        return items.reduce(function (menu, item, index) {
            if (item.group !== currentGroup) {
                if (index > 0) {
                    menu.push(React.createElement("div", { key: "d" + index, className: "dropdown-divider" }));
                }
                if (showGroupTitle) {
                    menu.push(React.createElement("h6", { key: "h" + index, className: "dropdown-header" }, item.group));
                }
                currentGroup = item.group ? item.group : null;
            }
            menu.push(_this.renderMenuItem(item, index, showCodeWithName != null ? showCodeWithName : false));
            return menu;
        }, []);
    };
    DropDownBase.prototype.renderMenuItem = function (item, index, showCodeWithName) {
        var _this = this;
        var selectedIndex = this.state.selectedIndex;
        return (React.createElement("button", { key: index, type: "button", className: cn('dropdown-item', index === selectedIndex && 'active'), tabIndex: -1, onMouseDown: function (e) { return _this.handleItemClick(e, item); } }, (showCodeWithName != null && showCodeWithName === true ? '(' + item.code + ') ' : '') + item.name));
    };
    DropDownBase.defaultProps = {
        emptyMessage: messages.noItems,
        showGroupTitle: true,
        focusOnItemClick: true
    };
    return DropDownBase;
}(React.Component));
export { DropDownBase };
//# sourceMappingURL=DropDownBase.js.map