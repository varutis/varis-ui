/// <reference types="react" />
import * as React from 'react';
import { DropDownItem } from './DropDownBase';
import { FieldProps } from '.';
export interface DropDownInputProps {
    name: string;
    form?: string;
    className?: string;
    disabled?: boolean;
    description?: string;
    placeholder?: string;
    multiSection?: boolean;
    showGroupTitle?: boolean;
    focusOnItemClick?: boolean;
    addons?: React.ReactElement<{}>[];
    size?: 'sm' | 'lg';
    options: DropDownItem[];
    showCodeWithName?: boolean;
    value?: number | string | null;
    onFocus?: React.FocusEventHandler<{}>;
    onBlur?: (e: {}, newValue: any) => void;
    onChange?: (e: {}, newValue: any) => void;
}
export interface DropDownInputState {
    items: DropDownItem[];
}
export declare class DropDownInput extends React.Component<DropDownInputProps, DropDownInputState> {
    private component;
    private selectedValue;
    constructor(props: DropDownInputProps);
    componentWillReceiveProps(nextProps: DropDownInputProps): void;
    componentDidMount(): void;
    focus(): void;
    parse(value: DropDownItem | null): any;
    format(value: number | string | null | undefined): DropDownItem | null;
    datasource: (query: string) => DropDownItem[];
    isUnknownValue(option: DropDownItem | null): boolean | null;
    validateValue(selectedValue: DropDownItem | null): void;
    handleBlur: (newValue: DropDownItem | null) => void;
    handleChange: (newValue: DropDownItem | null) => void;
    handleOpen: (text: string) => void;
    handleSearch: (text: string) => void;
    render(): JSX.Element;
}
export default DropDownInput;
export declare const DropDownField: React.ComponentClass<FieldProps & DropDownInputProps>;
