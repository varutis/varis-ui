var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import formField from './formField';
import { DropDownBase } from './DropDownBase';
var DropDownInput = /** @class */ (function (_super) {
    __extends(DropDownInput, _super);
    function DropDownInput(props) {
        var _this = _super.call(this, props) || this;
        _this.datasource = function (query) {
            if (!query || _this.props.options.length === 0) {
                return _this.props.options;
            }
            var q = query.toLowerCase();
            return _this.props.options.filter(function (x) { return x.name.substr(0, q.length).toLowerCase() === q; });
        };
        _this.handleBlur = function (newValue) {
            _this.selectedValue = _this.parse(newValue);
            if (_this.props.onBlur) {
                _this.props.onBlur(_this.selectedValue, undefined); // tslint:disable-line no-any
            }
        };
        _this.handleChange = function (newValue) {
            _this.selectedValue = _this.parse(newValue);
            if (_this.props.onChange) {
                _this.props.onChange(_this.selectedValue, undefined); // tslint:disable-line no-any
            }
        };
        _this.handleOpen = function (text) {
            _this.setState({ items: _this.props.options });
        };
        _this.handleSearch = function (text) {
            if (text === '') {
                _this.setState({ items: _this.props.options });
                return;
            }
            var normalizedText = text.toLowerCase();
            var items = _this.props.options.filter(function (item) { return item.name.substr(0, normalizedText.length).toLowerCase() === normalizedText; });
            _this.setState({ items: items });
        };
        _this.selectedValue = _this.format(_this.props.value);
        _this.state = {
            items: []
        };
        return _this;
    }
    DropDownInput.prototype.componentWillReceiveProps = function (nextProps) {
        this.selectedValue = this.format(nextProps.value);
    };
    DropDownInput.prototype.componentDidMount = function () {
        this.validateValue(this.selectedValue);
    };
    DropDownInput.prototype.focus = function () {
        if (this.component) {
            this.component.focus();
        }
    };
    DropDownInput.prototype.parse = function (value) {
        return value ? value.id : null;
    };
    DropDownInput.prototype.format = function (value) {
        if (value == null || value === '') {
            return null;
        }
        var option = this.props.options.find(function (x) { return x.id === value; });
        if (!option) {
            return { id: '-999999999', name: '', code: null, type: null, group: null };
        }
        return option;
    };
    DropDownInput.prototype.isUnknownValue = function (option) {
        return option && option.id === '-999999999';
    };
    DropDownInput.prototype.validateValue = function (selectedValue) {
        if (this.isUnknownValue(selectedValue)) {
            this.handleChange(selectedValue);
            this.handleBlur(selectedValue);
        }
    };
    DropDownInput.prototype.render = function () {
        var _this = this;
        var _a = this.props, className = _a.className, disabled = _a.disabled, name = _a.name, form = _a.form, description = _a.description, placeholder = _a.placeholder, multiSection = _a.multiSection, showGroupTitle = _a.showGroupTitle, addons = _a.addons, size = _a.size, focusOnItemClick = _a.focusOnItemClick, onFocus = _a.onFocus, showCodeWithName = _a.showCodeWithName;
        return (React.createElement(DropDownBase, { ref: function (x) { return _this.component = x; }, name: name, form: form, className: className, disabled: disabled, description: description, placeholder: placeholder, multiSection: multiSection, showGroupTitle: showGroupTitle, addons: addons, size: size, iconName: "fa-caret-down", focusOnItemClick: focusOnItemClick, items: this.state.items, showCodeWithName: showCodeWithName, value: this.selectedValue, onFocus: onFocus, onBlur: this.handleBlur, onChange: this.handleChange, onOpen: this.handleOpen, onSearch: this.handleSearch }));
    };
    return DropDownInput;
}(React.Component));
export { DropDownInput };
export default DropDownInput;
export var DropDownField = formField(DropDownInput);
//# sourceMappingURL=DropDownInput.js.map