/// <reference types="react" />
import { ReactElement } from 'react';
export interface InputProps {
    name: string;
    form?: string;
    component: ReactElement<{}>;
    addons?: ReactElement<{}>[];
    description?: string;
    size?: 'sm' | 'lg';
}
export declare const Input: ({ name, form, component, addons, description, size }: InputProps) => ReactElement<{}>;
export default Input;
