import { createElement } from 'react';
import * as cn from 'classnames';
import { Tooltip } from 'react-tippy';
export var Input = function (_a) {
    var name = _a.name, _b = _a.form, form = _b === void 0 ? '' : _b, component = _a.component, addons = _a.addons, description = _a.description, size = _a.size;
    var hasAddons = addons && addons.length > 0;
    var hasDescription = description && description.length > 0;
    if (!hasAddons && !hasDescription) {
        return component;
    }
    var props = {
        className: cn('input-group', size === 'sm' && 'input-group-sm', size === 'lg' && 'input-group-lg', hasAddons && 'input-group-with-addon', hasDescription && 'input-group-with-desc')
    };
    var children = [component];
    if (hasAddons) {
        children.push.apply(children, addons);
    }
    // console.warn(component);
    if (hasDescription) {
        children.push(createElement('span', { key: 'description', className: 'input-group-append text-muted' }, createElement(Tooltip, { key: 'tooltip', title: description, duration: 0, delay: [100, 0], arrow: true }, createElement('i', {
            // tslint:disable-next-line: max-line-length
            className: (component.type === 'textarea' ? 'text-area-info-icon' : 'input-info-icon') + " fas fa-info-circle fa-lg input-group-text px-1 d-flex align-items-center date-time-info-iconfas fa-info-circle fa-lg input-group-text px-1 d-flex align-items-center info-icon"
        }))));
    }
    return createElement('div', props, children);
};
export default Input;
//# sourceMappingURL=Input.js.map