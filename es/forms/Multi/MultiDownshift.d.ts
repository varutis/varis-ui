/// <reference types="react" />
import * as React from 'react';
import { GetItemPropsOptions, getToggleButtonPropsOptions } from 'downshift';
import { TagModel } from './TagValue';
import { FieldProps } from '..';
import { DropDownItem } from '../DropDownBase';
export interface MultiDownshiftProps {
    name: string;
    form?: string;
    description?: string;
    value?: DropDownItem[] | null;
    onChange?: (e: {}, newValue: any) => void;
    onSearch: (text: string) => Promise<DropDownItem[]>;
}
export interface MultiDownshiftState {
    inputValue: string;
    loading: boolean;
    items: DropDownItem[];
    isOpen: boolean;
}
export interface MultiDownshiftCancelationToken {
    canceled: boolean;
    cancel(): void;
}
export declare class MultiDownshift extends React.Component<MultiDownshiftProps, MultiDownshiftState> {
    private inputRef;
    private inputWrapperRef;
    private selectedItems;
    private cancelationToken;
    constructor(props: MultiDownshiftProps);
    componentWillReceiveProps(nextProps: MultiDownshiftProps): void;
    format(value: DropDownItem[] | null | undefined): DropDownItem[];
    handleStateChange: (changes: any, downshiftState: any) => void;
    handleOnSelect: (selectedItem: DropDownItem) => void;
    handleItemToString: (i: DropDownItem) => string;
    onWrapperClick: (e: React.MouseEvent<{}>) => void;
    focusOnInput(): void;
    handleSearch: any;
    processSearch(text: string, cancelationToken: MultiDownshiftCancelationToken): Promise<void>;
    handleOnTagBlur: (e: {}, tag: TagModel) => void;
    handleOnTagRemove: (tag: TagModel) => void;
    handleOnInputChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleOnInputKeyDown: (e: React.KeyboardEvent<{}>) => void;
    renderButton(isOpen: boolean, getToggleButtonProps: (options?: getToggleButtonPropsOptions | undefined) => any): JSX.Element;
    renderMenu(items: DropDownItem[], getItemProps: (options: GetItemPropsOptions<any>) => any, highlightedIndex: number | null, selectedItem: DropDownItem): JSX.Element;
    renderMenuItem(item: DropDownItem, index: number, getItemProps: (options: GetItemPropsOptions<any>) => any, highlightedIndex: number | null, selectedItem: DropDownItem): JSX.Element;
    render(): JSX.Element;
}
export default MultiDownshift;
export declare const MultiDownshiftField: React.ComponentClass<FieldProps & MultiDownshiftProps>;
