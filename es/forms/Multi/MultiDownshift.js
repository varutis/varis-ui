var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
var debounce = require('lodash.debounce');
import Downshift from 'downshift';
import AutosizeInput from 'react-input-autosize';
import { TagValue } from './TagValue';
// import matchSorter from 'match-sorter';
import formField from '../formField';
import Input from '../Input';
import { ButtonStyle, MenuStyle, MenuItemStyle, WrapperStyle, InputStyle } from './Styles';
function createCancelationToken() {
    return {
        canceled: false,
        cancel: function () {
            this.canceled = true;
        }
    };
}
var MultiDownshift = /** @class */ (function (_super) {
    __extends(MultiDownshift, _super);
    function MultiDownshift(props) {
        var _this = _super.call(this, props) || this;
        _this.cancelationToken = createCancelationToken();
        /// DOWNSHIFT:
        _this.handleStateChange = function (changes, downshiftState) {
            if (!downshiftState.isOpen) {
                _this.setState({ inputValue: '' });
            }
            if (changes.hasOwnProperty('inputValue') && typeof changes.inputValue === 'string') {
                var inputValue = changes.inputValue;
                _this.handleSearch(inputValue);
                _this.setState({ isOpen: true });
            }
        };
        _this.handleOnSelect = function (selectedItem) {
            _this.selectedItems = _this.selectedItems.concat([selectedItem]);
            if (_this.props.onChange) {
                _this.props.onChange(_this.selectedItems, undefined); // tslint:disable-line no-any
                _this.setState({ isOpen: false });
            }
        };
        _this.handleItemToString = function (i) {
            return i ? i.name : '';
        };
        /// WRAPPER:
        _this.onWrapperClick = function (e) {
            if (_this.inputWrapperRef === e.target || _this.inputRef === e.target) {
                _this.focusOnInput();
                e.stopPropagation();
                e.preventDefault();
            }
        };
        /// SEARCH
        // tslint:disable-next-line member-ordering
        _this.handleSearch = debounce(function (text) {
            _this.setState({ isOpen: true });
            _this.cancelationToken.cancel();
            _this.cancelationToken = createCancelationToken();
            _this.processSearch(text, _this.cancelationToken);
        }, 200);
        /// TAG:
        _this.handleOnTagBlur = function (e, tag) {
            var copy = _this.selectedItems.slice();
            copy.splice(tag.index, 1, tag.value);
            _this.selectedItems = copy;
            if (_this.props.onChange) {
                _this.props.onChange(_this.selectedItems, undefined); // tslint:disable-line no-any
            }
        };
        _this.handleOnTagRemove = function (tag) {
            var copy = _this.selectedItems.slice();
            copy.splice(tag.index, 1);
            _this.selectedItems = copy;
            if (_this.props.onChange) {
                _this.props.onChange(_this.selectedItems, undefined); // tslint:disable-line no-any
            }
        };
        /// INPUT:
        _this.handleOnInputChange = function (e) {
            _this.setState({ inputValue: e.target.value });
        };
        _this.handleOnInputKeyDown = function (e) {
            switch (e.keyCode) {
                case 8: // backspace
                case 46:// backspace
                    if (!_this.state.inputValue) {
                        e.preventDefault();
                        _this.handleOnTagRemove({ value: _this.selectedItems[_this.selectedItems.length - 1], index: _this.selectedItems.length - 1 });
                    }
                    return;
                default:
                    return;
            }
        };
        _this.selectedItems = _this.format(_this.props.value);
        _this.state = {
            inputValue: '',
            loading: false,
            items: [],
            isOpen: false
        };
        return _this;
    }
    MultiDownshift.prototype.componentWillReceiveProps = function (nextProps) {
        this.selectedItems = this.format(nextProps.value);
    };
    MultiDownshift.prototype.format = function (value) {
        if (!value) {
            return [];
        }
        return value;
    };
    MultiDownshift.prototype.focusOnInput = function () {
        if (this.inputRef) {
            var inputRef_1 = this.inputRef;
            setTimeout(function () { inputRef_1.focus(); inputRef_1.select(); }, 0);
            this.setState({ isOpen: true });
            this.handleSearch(this.state.inputValue);
        }
    };
    MultiDownshift.prototype.processSearch = function (text, cancelationToken) {
        return __awaiter(this, void 0, void 0, function () {
            var items, ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.setState({ loading: true, items: [] });
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.props.onSearch(text)];
                    case 2:
                        items = _a.sent();
                        if (!cancelationToken.canceled) {
                            this.setState({ loading: false, items: items });
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        ex_1 = _a.sent();
                        if (!cancelationToken.canceled) {
                            this.setState({ loading: false });
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MultiDownshift.prototype.renderButton = function (isOpen, getToggleButtonProps // tslint:disable-line:no-any
    ) {
        var _this = this;
        return (React.createElement("button", __assign({}, getToggleButtonProps(), { style: ButtonStyle, onClick: function () {
                _this.setState({ isOpen: !_this.state.isOpen });
                _this.handleSearch(_this.state.inputValue);
            } }),
            React.createElement("i", { className: 'fas fa-search' })));
    };
    MultiDownshift.prototype.renderMenu = function (items, getItemProps, // tslint:disable-line:no-any
    highlightedIndex, selectedItem) {
        var _this = this;
        return (React.createElement("div", { style: MenuStyle }, items.map(function (item, index) { return (_this.renderMenuItem(item, index, getItemProps, highlightedIndex, selectedItem)); })));
    };
    MultiDownshift.prototype.renderMenuItem = function (item, index, getItemProps, // tslint:disable-line:no-any
    highlightedIndex, selectedItem) {
        return (React.createElement("div", __assign({ key: "item-" + index }, getItemProps({
            item: item,
            index: index
        }), { style: MenuItemStyle(item, index, highlightedIndex, selectedItem) }), item.name));
    };
    MultiDownshift.prototype.render = function () {
        var _this = this;
        var _a = this.props, onChange = _a.onChange, onSearch = _a.onSearch, rest = __rest(_a, ["onChange", "onSearch"]);
        var items = this.state.items;
        return (React.createElement(Downshift, __assign({ onStateChange: this.handleStateChange, itemToString: this.handleItemToString, onSelect: this.handleOnSelect, selectedItem: this.selectedItems, isOpen: this.state.isOpen, onOuterClick: function () { return _this.setState({ isOpen: false }); } }, rest), function (_a) {
            var getInputProps = _a.getInputProps, getToggleButtonProps = _a.getToggleButtonProps, getItemProps = _a.getItemProps, isOpen = _a.isOpen, selectedItem = _a.selectedItem, highlightedIndex = _a.highlightedIndex;
            var _inputProps = getInputProps({
                value: _this.state.inputValue,
                onChange: _this.handleOnInputChange,
                onKeyDown: _this.handleOnInputKeyDown
            });
            var tagItems = selectedItem.map(function (item, index) { return ({ value: item, index: index }); });
            return (React.createElement("div", null,
                React.createElement("div", { style: { position: 'relative' } },
                    React.createElement("div", { ref: function (x) { return _this.inputWrapperRef = x; }, onClick: _this.onWrapperClick, tabIndex: -1, style: WrapperStyle },
                        tagItems.map(function (tag) { return (React.createElement(TagValue, { key: "Tag-" + tag.index, onBlur: _this.handleOnTagBlur, onRemove: _this.handleOnTagRemove, tag: tag })); }),
                        React.createElement(Input, { name: _this.props.name, form: _this.props.form, component: (React.createElement(AutosizeInput, __assign({}, _inputProps, { id: _this.props.name, ref: function (x) { return _this.inputRef = x; }, inputStyle: InputStyle }))), description: _this.props.description })),
                    _this.renderButton(isOpen, getToggleButtonProps)),
                !isOpen ? null : (_this.renderMenu(items, getItemProps, highlightedIndex, selectedItem))));
        }));
    };
    return MultiDownshift;
}(React.Component));
export { MultiDownshift };
export default MultiDownshift;
export var MultiDownshiftField = formField(MultiDownshift);
//# sourceMappingURL=MultiDownshift.js.map