/// <reference types="react" />
import * as React from 'react';
import { DropDownItem } from '../DropDownBase';
export interface TagModel {
    value: DropDownItem;
    index: number;
}
export interface TagValueProps {
    tag: TagModel;
    onRemove: (tag: TagModel) => void;
    onBlur: (e: {}, tag: TagModel) => void;
}
export interface TagValueState {
    value: DropDownItem;
}
export declare class TagValue extends React.Component<TagValueProps, TagValueState> {
    constructor(props: TagValueProps);
    componentWillReceiveProps(nextProps: TagValueProps): void;
    onBlur: (e: {}) => void;
    onRemove: (e: React.MouseEvent<{}>) => void;
    render(): JSX.Element;
}
