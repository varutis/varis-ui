var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import * as React from 'react';
var TagValue = /** @class */ (function (_super) {
    __extends(TagValue, _super);
    function TagValue(props) {
        var _this = _super.call(this, props) || this;
        _this.onBlur = function (e) {
            var _a = _this.props, onBlur = _a.onBlur, tag = _a.tag;
            if (onBlur) {
                onBlur(e, __assign({}, tag, { value: _this.state.value }));
            }
        };
        _this.onRemove = function (e) {
            var _a = _this.props, onRemove = _a.onRemove, tag = _a.tag;
            e.preventDefault();
            e.stopPropagation();
            if (onRemove) {
                onRemove(__assign({}, tag, { value: _this.state.value }));
            }
        };
        _this.state = {
            value: props.tag.value
        };
        return _this;
    }
    TagValue.prototype.componentWillReceiveProps = function (nextProps) {
        if (nextProps.tag !== this.props.tag) {
            this.setState({ value: nextProps.tag.value });
        }
    };
    TagValue.prototype.render = function () {
        var value = this.state.value;
        return (React.createElement("div", { style: { marginRight: 4, backgroundColor: '#ccc', padding: 2 } },
            value.name,
            ' ',
            React.createElement("span", { style: { cursor: 'pointer' }, onClick: this.onRemove },
                React.createElement("i", { className: "fas fa-times" }))));
    };
    return TagValue;
}(React.Component));
export { TagValue };
//# sourceMappingURL=TagValue.js.map