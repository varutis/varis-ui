/// <reference types="react" />
import { TextInputProps } from './TextInput';
import { FieldProps } from '.';
export declare const NumberField: React.ComponentClass<FieldProps & TextInputProps>;
