import { TextInput } from './TextInput';
import formField from './formField';
var numberFieldOptions = {
    format: function (value) {
        return value == null ? '' : typeof (value) === 'number' ? value.toString().replace('.', ',') : value;
    },
    normalize: function (value) {
        return typeof (value) === 'string' && value.indexOf('.') > 0 ? value.replace('.', ',') : value;
    }
};
export var NumberField = formField(TextInput, numberFieldOptions);
//# sourceMappingURL=NumberInput.js.map