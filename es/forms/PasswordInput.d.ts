/// <reference types="react" />
import * as React from 'react';
import { FieldProps } from '.';
export interface PasswordInputProps {
    name: string;
    addon?: React.ReactElement<{}>;
    autoComplete?: string;
    autoFocus?: boolean;
    className?: string;
    description?: string;
    disabled?: boolean;
    form?: string;
    maxLength?: number;
    placeholder?: string;
    value?: string | number;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
}
export declare class PasswordInput extends React.Component<PasswordInputProps> {
    render(): JSX.Element;
}
export default PasswordInput;
export declare const PasswordField: React.ComponentClass<FieldProps & PasswordInputProps>;
