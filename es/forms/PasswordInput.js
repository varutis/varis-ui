var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import formField from './formField';
import Input from './Input';
var PasswordInput = /** @class */ (function (_super) {
    __extends(PasswordInput, _super);
    function PasswordInput() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PasswordInput.prototype.render = function () {
        return (React.createElement(Input, { name: this.props.name, form: this.props.form, component: (React.createElement("input", { key: "component", id: this.props.name, autoComplete: this.props.autoComplete, autoFocus: this.props.autoFocus, className: 'form-control' + (this.props.className ? " " + this.props.className : ''), disabled: this.props.disabled, maxLength: this.props.maxLength, placeholder: this.props.placeholder, type: "password", value: this.props.value, onBlur: this.props.onBlur, onChange: this.props.onChange, onFocus: this.props.onFocus })), addons: this.props.addon ? [this.props.addon] : undefined, description: this.props.description }));
    };
    return PasswordInput;
}(React.Component));
export { PasswordInput };
export default PasswordInput;
export var PasswordField = formField(PasswordInput);
//# sourceMappingURL=PasswordInput.js.map