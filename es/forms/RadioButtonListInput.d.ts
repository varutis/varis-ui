/// <reference types="react" />
import * as React from 'react';
import { OptionWithDescription, ValueEvent } from '../model';
import { FieldProps } from '.';
export interface RadioButtonListInputProps {
    name: string;
    options: OptionWithDescription<number | string>[];
    className?: string;
    description?: string;
    disabled?: boolean;
    form?: string;
    value?: number | string;
    autoComplete?: string;
    onBlur?: ValueEvent;
    onFocus?: React.FocusEventHandler<{}>;
    onChange?: ValueEvent;
    inlineForm?: boolean;
}
export declare class RadioButtonListInput extends React.Component<RadioButtonListInputProps> {
    handleBlur: (e: React.FocusEvent<HTMLInputElement>) => void;
    handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    render(): JSX.Element;
}
export default RadioButtonListInput;
export declare const RadioButtonListField: React.ComponentClass<FieldProps & RadioButtonListInputProps>;
