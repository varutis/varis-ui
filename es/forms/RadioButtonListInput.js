var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import * as cn from 'classnames';
import { Tooltip } from 'react-tippy';
import formField from './formField';
import { nameToId } from '../utils';
var RadioButtonListInput = /** @class */ (function (_super) {
    __extends(RadioButtonListInput, _super);
    function RadioButtonListInput() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleBlur = function (e) {
            if (_this.props.onBlur) {
                _this.props.onBlur(e.currentTarget.value);
            }
        };
        _this.handleChange = function (e) {
            if (_this.props.onChange && e.target.checked) {
                _this.props.onChange(e.target.value);
            }
        };
        return _this;
    }
    RadioButtonListInput.prototype.render = function () {
        var _this = this;
        var _a = this.props, className = _a.className, description = _a.description, disabled = _a.disabled, name = _a.name, form = _a.form, options = _a.options, value = _a.value, inlineForm = _a.inlineForm, onFocus = _a.onFocus, autoComplete = _a.autoComplete;
        var inline = inlineForm === undefined ? true : inlineForm;
        return (React.createElement("div", null,
            options.map(function (x) { return (React.createElement("div", { key: x.id, className: cn('form-check', inline && 'form-check-inline', className) },
                React.createElement("label", { className: "form-check-label" },
                    React.createElement("input", { type: "radio", autoComplete: autoComplete, name: name, className: "form-check-input", disabled: disabled, value: x.id, checked: x.id === value || x.id.toString() === value, onBlur: _this.handleBlur, onChange: _this.handleChange, onFocus: onFocus }),
                    x.name,
                    x.description && React.createElement(Tooltip, { title: x.description, position: "top", duration: 0, delay: [100, 0], arrow: true, key: "item_tooltip_" + nameToId(name) },
                        React.createElement("i", { key: "itemDescription", id: "" + form + (form && '_') + nameToId(name) + "_item_descr", className: "ml-2 fas fa-info-circle" }))))); }),
            description && React.createElement(Tooltip, { title: description, position: "top", duration: 0, delay: [100, 0], arrow: true, key: "tooltip_" + nameToId(name) },
                React.createElement("i", { key: "description", id: "" + form + (form && '_') + nameToId(name) + "_descr", className: "ml-2 fas fa-info-circle" }))));
    };
    return RadioButtonListInput;
}(React.Component));
export { RadioButtonListInput };
export default RadioButtonListInput;
export var RadioButtonListField = formField(RadioButtonListInput);
//# sourceMappingURL=RadioButtonListInput.js.map