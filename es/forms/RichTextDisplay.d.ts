/// <reference types="react" />
export interface RichTextDisplayProps {
    value: string;
    plain?: boolean;
}
export declare const RichTextDisplay: ({ value, plain }: RichTextDisplayProps) => JSX.Element | null;
export default RichTextDisplay;
