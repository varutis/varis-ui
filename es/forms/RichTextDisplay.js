import * as React from 'react';
export var RichTextDisplay = function (_a) {
    var value = _a.value, plain = _a.plain;
    if (!value) {
        return null;
    }
    return (React.createElement("div", { dangerouslySetInnerHTML: { __html: value } }));
};
export default RichTextDisplay;
//# sourceMappingURL=RichTextDisplay.js.map