/// <reference types="react" />
import * as React from 'react';
import { FieldProps } from '.';
export interface TextAreaInputProps {
    name: string;
    addon?: React.ReactElement<{}>;
    autoComplete?: string;
    autoFocus?: boolean;
    className?: string;
    description?: string;
    disabled?: boolean;
    form?: string;
    maxLength?: number;
    placeholder?: string;
    rows?: number;
    value?: string | number;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
}
export declare class TextAreaInput extends React.Component<TextAreaInputProps> {
    render(): JSX.Element;
}
export default TextAreaInput;
export declare const TextAreaField: React.ComponentClass<FieldProps & TextAreaInputProps>;
