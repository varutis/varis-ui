var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import formField from './formField';
import Input from './Input';
var TextAreaInput = /** @class */ (function (_super) {
    __extends(TextAreaInput, _super);
    function TextAreaInput() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextAreaInput.prototype.render = function () {
        return (React.createElement(Input, { name: this.props.name, form: this.props.form, component: (React.createElement("textarea", { key: "component", id: this.props.name, autoComplete: this.props.autoComplete, autoFocus: this.props.autoFocus, className: 'form-control' + (this.props.className ? " " + this.props.className : ''), disabled: this.props.disabled, maxLength: this.props.maxLength, placeholder: this.props.placeholder, rows: this.props.rows, value: this.props.value, onBlur: this.props.onBlur, onChange: this.props.onChange, onFocus: this.props.onFocus })), addons: this.props.addon ? [this.props.addon] : undefined, description: this.props.description }));
    };
    return TextAreaInput;
}(React.Component));
export { TextAreaInput };
export default TextAreaInput;
export var TextAreaField = formField(TextAreaInput);
//# sourceMappingURL=TextAreaInput.js.map