/// <reference types="react" />
import * as React from 'react';
import { FieldProps } from '.';
export interface TextInputProps {
    name: string;
    addon?: React.ReactElement<{}>;
    autoComplete?: string;
    autoFocus?: boolean;
    className?: string;
    description?: string;
    disabled?: boolean;
    readonly?: boolean;
    form?: string;
    maxLength?: number;
    placeholder?: string;
    value?: string | number;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
}
export declare class TextInput extends React.Component<TextInputProps> {
    render(): JSX.Element;
}
export default TextInput;
export declare const TextField: React.ComponentClass<FieldProps & TextInputProps>;
