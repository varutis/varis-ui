var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import formField from './formField';
import Input from './Input';
var TextInput = /** @class */ (function (_super) {
    __extends(TextInput, _super);
    function TextInput() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextInput.prototype.render = function () {
        return (React.createElement(Input, { name: this.props.name, form: this.props.form, component: (React.createElement("input", { key: "component", id: this.props.name, autoFocus: this.props.autoFocus, autoComplete: this.props.autoComplete, className: 'form-control' + (this.props.className ? " " + this.props.className : ''), disabled: this.props.disabled, readOnly: this.props.readonly, maxLength: this.props.maxLength, placeholder: this.props.placeholder, type: "text", value: this.props.value, onBlur: this.props.onBlur, onChange: this.props.onChange, onFocus: this.props.onFocus })), addons: this.props.addon ? [this.props.addon] : undefined, description: this.props.description }));
    };
    return TextInput;
}(React.Component));
export { TextInput };
export default TextInput;
export var TextField = formField(TextInput);
//# sourceMappingURL=TextInput.js.map