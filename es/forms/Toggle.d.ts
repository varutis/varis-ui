/// <reference types="react" />
import * as React from 'react';
import { FieldProps } from '.';
export declare type ToggleProps = {
    name: string;
    className?: string;
    description?: string;
    form?: string;
    value?: boolean;
    disabled?: boolean;
    autoFocus?: boolean;
    autoComplete?: string;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
};
export declare class Toggle extends React.Component<ToggleProps> {
    static parse: (value: string | boolean) => boolean;
    render(): JSX.Element;
}
export default Toggle;
export declare const ToggleField: React.ComponentClass<FieldProps & ToggleProps>;
