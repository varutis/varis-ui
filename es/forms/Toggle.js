var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { Tooltip } from 'react-tippy';
import formField from './formField';
import { nameToId } from '../utils';
var Toggle = /** @class */ (function (_super) {
    __extends(Toggle, _super);
    function Toggle() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Toggle.prototype.render = function () {
        var _a = this.props, autoFocus = _a.autoFocus, className = _a.className, description = _a.description, disabled = _a.disabled, name = _a.name, form = _a.form, value = _a.value, onBlur = _a.onBlur, onChange = _a.onChange, onFocus = _a.onFocus, autoComplete = _a.autoComplete;
        return (React.createElement("span", { className: className },
            React.createElement("label", { className: "switch" },
                React.createElement("input", { id: name, autoComplete: autoComplete, type: "checkbox", className: "form-check-input", disabled: disabled, autoFocus: autoFocus, checked: !!value, onBlur: onBlur, onChange: onChange, onFocus: onFocus }),
                React.createElement("span", { className: "slider round pt-3" })),
            description && React.createElement(Tooltip, { title: description, position: "top", duration: 0, delay: [100, 0], arrow: true, key: "tooltip_" + nameToId(name) },
                React.createElement("i", { key: "description", id: "" + form + (form && '_') + nameToId(name) + "_descr", className: "ml-2 align-top fas fa-info-circle" }))));
    };
    Toggle.parse = function (value) { return !!value; };
    return Toggle;
}(React.Component));
export { Toggle };
export default Toggle;
export var ToggleField = formField(Toggle);
//# sourceMappingURL=Toggle.js.map