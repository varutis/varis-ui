/// <reference types="react" />
import * as React from 'react';
import { DropDownItem } from './DropDownBase';
import { FieldProps } from '.';
export interface TypeaheadInputProps {
    name: string;
    form?: string;
    className?: string;
    disabled?: boolean;
    description?: string;
    placeholder?: string;
    multiSection?: boolean;
    showGroupTitle?: boolean;
    focusOnItemClick?: boolean;
    addons?: React.ReactElement<{}>[];
    size?: 'sm' | 'lg';
    value?: DropDownItem | null;
    onFocus?: React.FocusEventHandler<{}>;
    minSearchSymbols?: number;
    onBlur?: (e: {}, newValue: any) => void;
    onChange?: (e: {}, newValue: any) => void;
    onSearch: (text: string) => Promise<DropDownItem[]>;
}
export interface TypeaheadInputState {
    loading: boolean;
    items: DropDownItem[];
}
export interface TypeaheadCancelationToken {
    canceled: boolean;
    cancel(): void;
}
export declare class TypeaheadInput extends React.Component<TypeaheadInputProps, TypeaheadInputState> {
    private component;
    private selectedValue;
    private cancelationToken;
    constructor(props: TypeaheadInputProps);
    componentWillReceiveProps(nextProps: TypeaheadInputProps): void;
    focus(): void;
    format(value: DropDownItem | '' | null | undefined): DropDownItem | null;
    getMinSearchSymbols(): number;
    getEmptyMessage(): string;
    handleBlur: (newValue: DropDownItem | null) => void;
    handleChange: (newValue: DropDownItem | null) => void;
    handleSearch: any;
    processSearch(text: string, cancelationToken: TypeaheadCancelationToken): Promise<void>;
    render(): JSX.Element;
}
export default TypeaheadInput;
export declare const TypeaheadField: React.ComponentClass<FieldProps & TypeaheadInputProps>;
