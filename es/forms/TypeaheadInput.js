var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import * as React from 'react';
var debounce = require('lodash.debounce');
import { DropDownBase } from './DropDownBase';
import formField from './formField';
function createCancelationToken() {
    return {
        canceled: false,
        cancel: function () {
            this.canceled = true;
        }
    };
}
var TypeaheadInput = /** @class */ (function (_super) {
    __extends(TypeaheadInput, _super);
    function TypeaheadInput(props) {
        var _this = _super.call(this, props) || this;
        _this.cancelationToken = createCancelationToken();
        _this.handleBlur = function (newValue) {
            _this.selectedValue = newValue;
            if (_this.props.onBlur) {
                _this.props.onBlur(_this.selectedValue, undefined); // tslint:disable-line no-any
            }
        };
        _this.handleChange = function (newValue) {
            _this.selectedValue = newValue;
            if (_this.props.onChange) {
                _this.props.onChange(_this.selectedValue, undefined); // tslint:disable-line no-any
            }
        };
        // tslint:disable-next-line member-ordering
        _this.handleSearch = debounce(function (text) {
            if (text.length < _this.getMinSearchSymbols()) {
                _this.setState({ items: [] });
                return;
            }
            _this.cancelationToken.cancel();
            _this.cancelationToken = createCancelationToken();
            _this.processSearch(text, _this.cancelationToken);
        }, 200);
        _this.selectedValue = _this.format(_this.props.value);
        _this.state = {
            loading: false,
            items: []
        };
        return _this;
    }
    TypeaheadInput.prototype.componentWillReceiveProps = function (nextProps) {
        this.selectedValue = this.format(nextProps.value);
    };
    TypeaheadInput.prototype.focus = function () {
        if (this.component) {
            this.component.focus();
        }
    };
    TypeaheadInput.prototype.format = function (value) {
        if (value == null || value === '') {
            return null;
        }
        return value;
    };
    TypeaheadInput.prototype.getMinSearchSymbols = function () {
        return (this.props.minSearchSymbols !== undefined) ? this.props.minSearchSymbols : 3;
    };
    TypeaheadInput.prototype.getEmptyMessage = function () {
        var value = this.getMinSearchSymbols();
        if ((value > 9 && value < 20) || value % 10 === 0 || ((value % 100 > 9) && (value % 100 < 20))) {
            return "\u012Eveskite " + value + " simboli\u0173";
        }
        if (value === 1 || value !== 11 && (value % 100 !== 11 && value % 10 === 1)) {
            return "\u012Eveskite " + value + " simbol\u012F";
        }
        if (value > 1 && value < 10 || value > 21 && (value % 10 > 1 && value % 10 < 10)) {
            return "\u012Eveskite " + value + " simbolius";
        }
        return '';
    };
    TypeaheadInput.prototype.processSearch = function (text, cancelationToken) {
        return __awaiter(this, void 0, void 0, function () {
            var items, ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.setState({ loading: true, items: [] });
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.props.onSearch(text)];
                    case 2:
                        items = _a.sent();
                        if (!cancelationToken.canceled) {
                            this.setState({ loading: false, items: items });
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        ex_1 = _a.sent();
                        console.error("Error in " + this.props.name + " datasource", ex_1); // tslint:disable-line no-console
                        if (!cancelationToken.canceled) {
                            this.setState({ loading: false });
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    TypeaheadInput.prototype.render = function () {
        var _this = this;
        var _a = this.props, className = _a.className, disabled = _a.disabled, name = _a.name, form = _a.form, description = _a.description, placeholder = _a.placeholder, multiSection = _a.multiSection, showGroupTitle = _a.showGroupTitle, addons = _a.addons, size = _a.size, focusOnItemClick = _a.focusOnItemClick, onFocus = _a.onFocus;
        var _b = this.state, loading = _b.loading, items = _b.items;
        return (React.createElement(DropDownBase, { ref: function (x) { return _this.component = x; }, name: name, form: form, className: className, disabled: disabled, description: description, placeholder: placeholder, multiSection: multiSection, showGroupTitle: showGroupTitle, addons: addons, size: size, iconName: "fa-search", emptyMessage: this.getEmptyMessage(), minSearchSymbols: this.getMinSearchSymbols(), focusOnItemClick: focusOnItemClick, loading: loading, items: items, value: this.selectedValue, onFocus: onFocus, onBlur: this.handleBlur, onChange: this.handleChange, onSearch: this.handleSearch }));
    };
    return TypeaheadInput;
}(React.Component));
export { TypeaheadInput };
export default TypeaheadInput;
export var TypeaheadField = formField(TypeaheadInput);
//# sourceMappingURL=TypeaheadInput.js.map