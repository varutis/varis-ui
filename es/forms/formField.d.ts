/// <reference types="react" />
import * as React from 'react';
export interface ComponentProps {
    name: string;
}
export interface FieldProps {
    className?: string;
    label?: string;
    labelWidth?: number;
    rowLayout?: boolean;
    showErrorText?: boolean;
}
export interface WrappedField<T> {
    getRenderedComponent(): T;
}
export interface FormFieldOptions {
    format?: (value: any) => any;
    normalize?: (value: any) => any;
    parse?: (value: any) => any;
}
export declare function formField<P extends ComponentProps>(component: React.ComponentClass<P>, options?: FormFieldOptions): React.ComponentClass<FieldProps & P>;
export default formField;
