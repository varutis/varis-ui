var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
import * as cn from 'classnames';
import { Field } from 'redux-form';
export function formField(component, options) {
    var WrappedComponent = /** @class */ (function (_super) {
        __extends(WrappedComponent, _super);
        function WrappedComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        WrappedComponent.prototype.getRenderedComponent = function () {
            return this.component;
        };
        WrappedComponent.prototype.render = function () {
            var _this = this;
            var _a = this.props, input = _a.input, meta = _a.meta, className = _a.className, label = _a.label, _b = _a.labelWidth, labelWidth = _b === void 0 ? 4 : _b, _c = _a.rowLayout, rowLayout = _c === void 0 ? true : _c, _d = _a.showErrorText, showErrorText = _d === void 0 ? false : _d, rest = __rest(_a, ["input", "meta", "className", "label", "labelWidth", "rowLayout", "showErrorText"]); // P gives error in typescript
            var children = [];
            var hasLabel = typeof (label) !== 'undefined';
            if (hasLabel) {
                var labelProps = { key: 'label' };
                if (rowLayout) {
                    labelProps.className = "col-sm-" + labelWidth + " col-form-label";
                }
                children.push(React.createElement('label', labelProps, label));
            }
            var inputColumn = [];
            var isInvalid = meta.error && (meta.dirty || meta.touched || meta.submitFailed);
            inputColumn.push(React.createElement(component, __assign({ key: 'input', ref: function (x) { return _this.component = x; } }, input, rest, { form: meta.form, className: isInvalid ? (className || '') + " is-invalid" : className })));
            if (showErrorText && isInvalid) {
                inputColumn.push(React.createElement('div', { key: 'error', className: 'invalid-feedback' }, meta.error));
            }
            if (rowLayout && hasLabel) {
                children.push(React.createElement('div', { key: 'input', className: "col-sm-" + (12 - labelWidth) }, inputColumn));
            }
            else {
                children.push.apply(children, inputColumn);
            }
            var containerProps = {
                className: cn('form-group', {
                    'row': rowLayout && hasLabel,
                    'has-error': !!isInvalid
                })
            };
            return React.createElement('div', containerProps, children);
        };
        return WrappedComponent;
    }(React.Component));
    return /** @class */ (function (_super) {
        __extends(WrappedField, _super);
        function WrappedField() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        WrappedField.prototype.getRenderedComponent = function () {
            var wrappedComponent = this.field && this.field.getRenderedComponent(); // tslint:disable-line no-any
            return wrappedComponent && wrappedComponent.getRenderedComponent();
        };
        WrappedField.prototype.render = function () {
            var _this = this;
            return React.createElement(Field, __assign({}, this.props, { ref: function (x) { return _this.field = x; }, withRef: true, format: options && options.format, normalize: options && options.normalize, parse: options && options.parse, component: WrappedComponent }));
        };
        return WrappedField;
    }(React.Component));
}
export default formField;
//# sourceMappingURL=formField.js.map