import { BaseSchema } from './base';
export declare class AnySchema extends BaseSchema<any> {
    constructor();
    protected isOfType(value: any): value is any;
}
