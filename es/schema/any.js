var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// tslint:disable no-any
import { BaseSchema, isAbsent } from './base';
var AnySchema = /** @class */ (function (_super) {
    __extends(AnySchema, _super);
    function AnySchema() {
        var _this = _super.call(this) || this;
        _this.transform(function (v) { return v; });
        _this.addSyncRule(function (v) { return isAbsent(v) || _this.isOfType(v) ? undefined : _this.formatError('Įvesta informacija neatitinka jokio DataType tipo'); });
        return _this;
    }
    AnySchema.prototype.isOfType = function (value) {
        return true;
    };
    return AnySchema;
}(BaseSchema));
export { AnySchema };
//# sourceMappingURL=any.js.map