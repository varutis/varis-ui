export declare type ValidationContext = {
    parent: ValidationContext | undefined;
    container: any;
    key: string | number | undefined;
};
export declare type ValidationError = undefined | string | {
    [P: string]: ValidationError;
} | {
    [P: number]: ValidationError;
};
export declare type Transformation = (currentValue: Readonly<any>, originalValue: Readonly<any>) => any;
export declare type SyncRule<T> = (value: T | null | undefined, context: ValidationContext) => ValidationError;
export declare const isAbsent: (v: any) => v is null | undefined;
export declare abstract class BaseSchema<T> {
    private defaultLabel;
    private defaultValue;
    private transformations;
    private syncRules;
    constructor();
    protected addSyncRule(rule: SyncRule<T>): void;
    protected abstract isOfType(value: any): value is T;
    protected formatError(message: string): string;
    add(isValid: (value: T, context: ValidationContext) => boolean, message: string): this;
    addRule(isValid: (value: T | null | undefined, context: ValidationContext) => boolean, message: string): this;
    label(value: string): this;
    convert(value: any): T | null | undefined;
    default(value: T | null | undefined): this;
    transform(transformation: Transformation): this;
    validate(value: T | null | undefined, context?: ValidationContext): ValidationError;
}
