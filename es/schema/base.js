// tslint:disable no-any
export var isAbsent = function (v) { return v === null || v === undefined; };
var BaseSchema = /** @class */ (function () {
    function BaseSchema() {
        this.defaultValue = undefined;
        this.transformations = [];
        this.syncRules = [];
    }
    BaseSchema.prototype.addSyncRule = function (rule) {
        this.syncRules.push(rule);
    };
    BaseSchema.prototype.formatError = function (message) {
        return this.defaultLabel ? this.defaultLabel + ": " + (message.charAt(0).toLowerCase() + message.slice(1)) : message;
    };
    BaseSchema.prototype.add = function (isValid, message) {
        var _this = this;
        this.addSyncRule(function (v, context) { return isAbsent(v) || isValid(v, context)
            ? undefined : _this.formatError(message); });
        return this;
    };
    BaseSchema.prototype.addRule = function (isValid, message) {
        var _this = this;
        this.addSyncRule(function (v, context) { return isValid(v, context)
            ? undefined : _this.formatError(message); });
        return this;
    };
    BaseSchema.prototype.label = function (value) {
        this.defaultLabel = value;
        return this;
    };
    BaseSchema.prototype.convert = function (value) {
        var _this = this;
        if (isAbsent(value)) {
            return this.defaultValue !== undefined ? this.defaultValue : value;
        }
        return this.transformations.reduce(function (currentValue, fn) { return fn.call(_this, currentValue, value); }, value);
    };
    BaseSchema.prototype.default = function (value) {
        this.defaultValue = value;
        return this;
    };
    BaseSchema.prototype.transform = function (transformation) {
        this.transformations.push(transformation);
        return this;
    };
    BaseSchema.prototype.validate = function (value, context) {
        var _this = this;
        if (typeof context === 'undefined') {
            context = { parent: undefined, container: value, key: undefined };
        }
        var errors = this.syncRules.reduce(function (error, rule) { return error || rule.call(_this, value, context); }, undefined);
        // console.warn('VALIDATION', value, errors); // Leave this line commented for debugging purpose
        return errors;
    };
    return BaseSchema;
}());
export { BaseSchema };
//# sourceMappingURL=base.js.map