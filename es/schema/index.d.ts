import { ListSchema } from './list';
import { MomentSchema } from './moment';
import { NumberSchema } from './number';
import { ObjectSchema } from './object';
import { StringSchema } from './string';
import { AnySchema } from './any';
export { ValidationError } from './base';
export { reduxSchemaForm } from './reduxForm';
export { ListSchema, MomentSchema, NumberSchema, ObjectSchema, StringSchema, AnySchema };
export declare const schema: {
    list: <T>() => ListSchema<T>;
    moment: () => MomentSchema;
    number: () => NumberSchema;
    object: <T extends object>() => ObjectSchema<T>;
    string: <T extends string>() => StringSchema<T>;
    any: () => AnySchema;
};
export default schema;
