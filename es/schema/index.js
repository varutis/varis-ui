import { ListSchema } from './list';
import { MomentSchema } from './moment';
import { NumberSchema } from './number';
import { ObjectSchema } from './object';
import { StringSchema } from './string';
import { AnySchema } from './any';
export { reduxSchemaForm } from './reduxForm';
export { ListSchema, MomentSchema, NumberSchema, ObjectSchema, StringSchema, AnySchema };
export var schema = {
    list: function () { return new ListSchema(); },
    moment: function () { return new MomentSchema(); },
    number: function () { return new NumberSchema(); },
    object: function () { return new ObjectSchema(); },
    string: function () { return new StringSchema(); },
    any: function () { return new AnySchema(); },
};
export default schema;
//# sourceMappingURL=index.js.map