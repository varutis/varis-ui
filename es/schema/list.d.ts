import { BaseSchema } from './base';
export declare class ListSchema<T> extends BaseSchema<T[]> {
    private itemSchema;
    constructor();
    protected isOfType(value: any): value is T[];
    required(): this;
    min(minSize: number): this;
    max(maxSize: number): this;
    of(itemSchema: BaseSchema<T>): this;
}
