var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// tslint:disable no-any
import { BaseSchema, isAbsent } from './base';
var createInvalidEmptyValue = function () {
    var result = [];
    result['_isInvalid'] = true; // tslint:disable-line no-string-literal
    return result;
};
var createListError = function (message) {
    var error = [];
    error['_error'] = message; // tslint:disable-line no-string-literal
    return error;
};
var ListSchema = /** @class */ (function (_super) {
    __extends(ListSchema, _super);
    function ListSchema() {
        var _this = _super.call(this) || this;
        _this.transform(function (v) { return isAbsent(v) || _this.isOfType(v) ? v : createInvalidEmptyValue(); });
        _this.addSyncRule(function (v) { return isAbsent(v) || _this.isOfType(v) ? undefined : createListError(_this.formatError('Reikšmė turi būti sąrašas')); });
        return _this;
    }
    ListSchema.prototype.isOfType = function (value) {
        return value instanceof Array && !value['_isInvalid']; // tslint:disable-line no-string-literal
    };
    ListSchema.prototype.required = function () {
        var _this = this;
        this.addSyncRule(function (v) { return !isAbsent(v) ? undefined : createListError(_this.formatError('Reikšmė yra privaloma')); });
        return this;
    };
    ListSchema.prototype.min = function (minSize) {
        var _this = this;
        this.addSyncRule(function (v) { return isAbsent(v) || v.length >= minSize ?
            undefined : createListError(_this.formatError("S\u0105ra\u0161e turi b\u016Bti ne ma\u017Eiau kaip " + minSize + " \u012Fra\u0161as(-ai)")); });
        return this;
    };
    ListSchema.prototype.max = function (maxSize) {
        var _this = this;
        this.addSyncRule(function (v) { return isAbsent(v) || v.length <= maxSize ?
            undefined : createListError(_this.formatError("S\u0105ra\u0161e turi b\u016Bti ne daugiau kaip " + maxSize + " \u012Fra\u0161ai(-\u0173)")); });
        return this;
    };
    ListSchema.prototype.of = function (itemSchema) {
        var _this = this;
        this.itemSchema = itemSchema;
        this.transform(function (v) {
            if (!_this.isOfType(v)) {
                return v;
            }
            return v.map(function (item) { return _this.itemSchema.convert(item); });
        });
        this.addSyncRule(function (v, c) { return v.reduce(function (errors, item, index) {
            var context = { parent: c, container: v, key: index };
            var error = _this.itemSchema.validate(item, context);
            if (error) {
                errors = errors || [];
                errors[index] = error;
            }
            return errors;
        }, undefined); });
        return this;
    };
    return ListSchema;
}(BaseSchema));
export { ListSchema };
//# sourceMappingURL=list.js.map