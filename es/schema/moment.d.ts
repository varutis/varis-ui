import { Moment } from 'moment';
import { BaseSchema } from './base';
export declare class MomentSchema extends BaseSchema<Moment> {
    constructor();
    protected isOfType(value: any): value is Moment;
    required(): this;
}
