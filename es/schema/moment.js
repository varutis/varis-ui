var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// tslint:disable no-any
import * as moment from 'moment';
import { BaseSchema, isAbsent } from './base';
var MomentSchema = /** @class */ (function (_super) {
    __extends(MomentSchema, _super);
    function MomentSchema() {
        var _this = _super.call(this) || this;
        _this.transform(function (v) {
            return moment.isMoment(v) && !(v instanceof moment) ? moment(v) : (typeof (v) === 'string' && v.length === 0 ? null : v);
        });
        _this.addSyncRule(function (v) { return isAbsent(v) || _this.isOfType(v) ? undefined : _this.formatError('Reikšmė turi būti data'); });
        return _this;
    }
    MomentSchema.prototype.isOfType = function (value) {
        return moment.isMoment(value) && value.isValid() && !isNaN(value.toDate().getTime());
    };
    MomentSchema.prototype.required = function () {
        var _this = this;
        this.addSyncRule(function (v) { return !isAbsent(v) ? undefined : _this.formatError('Reikšmė yra privaloma'); });
        return this;
    };
    return MomentSchema;
}(BaseSchema));
export { MomentSchema };
//# sourceMappingURL=moment.js.map