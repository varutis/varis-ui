import { BaseSchema } from './base';
export declare class NumberSchema extends BaseSchema<number> {
    constructor();
    protected isOfType(value: any): value is number;
    integer(): this;
    maxFractionalDigits(value: number): this;
    min(minValue: number): this;
    max(maxValue: number): this;
    round(precision: number): this;
    optionMustExist(): this;
    required(): this;
}
