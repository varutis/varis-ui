var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// tslint:disable no-any
import { BaseSchema, isAbsent } from './base';
var parse = function (value) {
    if (value.indexOf(',') >= 0) {
        value = value.replace(',', '.');
    }
    var num = parseFloat(value);
    return !isNaN(num) && isFinite(value) ? num : NaN;
};
function decimalAdjust(value, exp) {
    if (exp === 0) {
        return Math.round(value);
    }
    if (value < 0) {
        return -decimalAdjust(-value, exp);
    }
    var shiftedValue = value.toString().split('e');
    value = Math.round(+(shiftedValue[0] + 'e' + (shiftedValue[1] ? (+shiftedValue[1] + exp) : exp)));
    var unshiftedValue = value.toString().split('e');
    return +(unshiftedValue[0] + 'e' + (unshiftedValue[1] ? (+unshiftedValue[1] - exp) : -exp));
}
function getDecimalCount(value) {
    if (!isAbsent(value)) {
        var splitValues = value.toString().split('.');
        return splitValues.length === 1 ? 0 : splitValues[1].length;
    }
    else {
        return 0;
    }
}
var NumberSchema = /** @class */ (function (_super) {
    __extends(NumberSchema, _super);
    function NumberSchema() {
        var _this = _super.call(this) || this;
        _this.transform(function (v) { return isAbsent(v) || _this.isOfType(v) ? v : typeof (v) !== 'string' ? NaN : v.length === 0 ? null : parse(v); });
        _this.addSyncRule(function (v) { return isAbsent(v) || _this.isOfType(v) ? undefined : _this.formatError('Reikšmė turi būti skaičius'); });
        return _this;
    }
    NumberSchema.prototype.isOfType = function (value) {
        return typeof (value) === 'number' && !isNaN(value);
    };
    NumberSchema.prototype.integer = function () {
        var _this = this;
        this.addSyncRule(function (v) { return isAbsent(v) || v === (v | 0) ? // tslint:disable-line no-bitwise
            undefined :
            _this.formatError('Reikšmė turi būti sveikasis skaičius'); });
        return this;
    };
    NumberSchema.prototype.maxFractionalDigits = function (value) {
        var _this = this;
        var delta = Math.pow(10, value);
        this.addSyncRule(function (v) {
            var decExp = getDecimalCount(v);
            return isAbsent(v) || (decimalAdjust(v * delta, decExp) === (decimalAdjust(v * delta, decExp) | 0)) ? // tslint:disable-line no-bitwise
                undefined :
                _this.formatError("Reik\u0161m\u0117 negali tur\u0117ti daugiau kaip " + value + " skai\u010Di\u0173 po kablelio");
        });
        return this;
    };
    NumberSchema.prototype.min = function (minValue) {
        var _this = this;
        this.addSyncRule(function (v) { return isAbsent(v) || v >= minValue ? undefined : _this.formatError("Reik\u0161m\u0117 turi b\u016Bti ne ma\u017Eesn\u0117 u\u017E " + minValue); });
        return this;
    };
    NumberSchema.prototype.max = function (maxValue) {
        var _this = this;
        this.addSyncRule(function (v) { return isAbsent(v) || v <= maxValue ? undefined : _this.formatError("Reik\u0161m\u0117 turi b\u016Bti ne didesn\u0117 u\u017E " + maxValue); });
        return this;
    };
    NumberSchema.prototype.round = function (precision) {
        var _this = this;
        this.transform(function (v) { return isAbsent(v) || !_this.isOfType(v) ? v : decimalAdjust(v, precision); });
        return this;
    };
    NumberSchema.prototype.optionMustExist = function () {
        var _this = this;
        this.addSyncRule(function (v) {
            return (!isAbsent(v) && _this.isOfType(v) && v.toString() === '-999999999') ?
                _this.formatError('Reikšmė neegzistuoja sąraše') :
                undefined;
        });
        return this;
    };
    NumberSchema.prototype.required = function () {
        var _this = this;
        this.addSyncRule(function (v) {
            return !isAbsent(v) ? undefined : _this.formatError('Reikšmė yra privaloma');
        });
        return this;
    };
    return NumberSchema;
}(BaseSchema));
export { NumberSchema };
//# sourceMappingURL=number.js.map