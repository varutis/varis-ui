import { BaseSchema } from './base';
export declare type ObjectShape<T extends object> = {
    [P in keyof T]?: BaseSchema<T[P]>;
};
export declare class ObjectSchema<T extends object> extends BaseSchema<T> {
    private shape;
    constructor();
    protected isOfType(value: any): value is T;
    of(shape: ObjectShape<T>): this;
    required(): this;
}
