var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// tslint:disable no-any
import { BaseSchema, isAbsent } from './base';
var ObjectSchema = /** @class */ (function (_super) {
    __extends(ObjectSchema, _super);
    function ObjectSchema() {
        var _this = _super.call(this) || this;
        _this.transform(function (v) { return isAbsent(v) || _this.isOfType(v) ? v : 'INVALID'; });
        _this.addSyncRule(function (v) { return isAbsent(v) || _this.isOfType(v) ? undefined : 'Reikšmė turi būti objektas'; });
        return _this;
    }
    ObjectSchema.prototype.isOfType = function (value) {
        return Object.prototype.toString.call(value) === '[object Object]';
    };
    ObjectSchema.prototype.of = function (shape) {
        var _this = this;
        this.shape = shape;
        this.transform(function (v) {
            if (!_this.isOfType(v)) {
                return v;
            }
            return Object.keys(_this.shape).reduce(function (value, key) {
                value[key] = _this.shape[key].convert(value[key]);
                return value;
            }, Object.assign({}, v));
        });
        this.addSyncRule(function (v, c) { return Object.keys(_this.shape).reduce(function (errors, key) {
            var context = { parent: c, container: v, key: key };
            var error = _this.shape[key].validate(v[key], context);
            if (error) {
                errors = errors || {};
                errors[key] = error;
            }
            return errors;
        }, undefined); });
        return this;
    };
    ObjectSchema.prototype.required = function () {
        var _this = this;
        this.addSyncRule(function (v) { return !isAbsent(v) ? undefined : _this.formatError('Reikšmė yra privaloma'); });
        return this;
    };
    return ObjectSchema;
}(BaseSchema));
export { ObjectSchema };
//# sourceMappingURL=object.js.map