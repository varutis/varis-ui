import { DataShape, Config } from 'redux-form';
import { ObjectSchema } from './object';
export declare function reduxSchemaForm<FormData extends DataShape, P, S>(schema: ObjectSchema<FormData>, config: Config<FormData, P, S>): <T>(component: T) => T;
