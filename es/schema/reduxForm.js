var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
// tslint:disable no-any
import * as React from 'react';
import { reduxForm } from 'redux-form';
export function reduxSchemaForm(schema, config) {
    var schemaConfig = __assign({}, config, { validate: function (values) { return schema.validate(schema.convert(values)); } });
    var createHandler = function (handler) { return function (values, dispatch, props) { return handler(schema.convert(values), dispatch, props); }; };
    var form = reduxForm(schemaConfig);
    return function (component) {
        var SchemaWrapper = /** @class */ (function (_super) {
            __extends(SchemaWrapper, _super);
            function SchemaWrapper() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.handleSubmit = function (handlerOrEvent) {
                    var originalHandleSubmit = _this.props.handleSubmit;
                    if (typeof (handlerOrEvent) === 'function') {
                        return originalHandleSubmit(createHandler(handlerOrEvent));
                    }
                    var onSubmit = _this.props.onSubmit;
                    if (typeof (onSubmit) !== 'function') {
                        return originalHandleSubmit(handlerOrEvent);
                    }
                    return originalHandleSubmit(createHandler(onSubmit))(handlerOrEvent);
                };
                return _this;
            }
            SchemaWrapper.prototype.render = function () {
                return React.createElement(component, Object.assign({}, this.props, {
                    handleSubmit: this.handleSubmit
                }));
            };
            return SchemaWrapper;
        }(React.Component));
        return form(SchemaWrapper);
    };
}
//# sourceMappingURL=reduxForm.js.map