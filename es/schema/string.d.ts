import { BaseSchema } from './base';
export declare class StringSchema<T extends string> extends BaseSchema<T> {
    constructor();
    protected isOfType(value: any): value is T;
    maxLength(length: number): this;
    optionMustExist(): this;
    required(): this;
}
