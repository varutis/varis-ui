var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// tslint:disable no-any
import { BaseSchema, isAbsent } from './base';
var StringSchema = /** @class */ (function (_super) {
    __extends(StringSchema, _super);
    function StringSchema() {
        var _this = _super.call(this) || this;
        _this.transform(function (v) { return !isAbsent(v) && v.toString ? v.toString() : v; });
        _this.addSyncRule(function (v) { return isAbsent(v) || _this.isOfType(v) ? undefined : 'Reikšmė turi būti tekstas'; });
        return _this;
    }
    StringSchema.prototype.isOfType = function (value) {
        return typeof (value) === 'string';
    };
    StringSchema.prototype.maxLength = function (length) {
        var _this = this;
        this.addSyncRule(function (v) { return isAbsent(v) || v.length <= length ?
            undefined :
            _this.formatError("Reik\u0161m\u0117 turi tur\u0117ti ne daugiau kaip " + length + " simboli\u0173"); });
        return this;
    };
    StringSchema.prototype.optionMustExist = function () {
        var _this = this;
        this.addSyncRule(function (v) {
            return (!isAbsent(v) && _this.isOfType(v) && v.toString() === '-999999999') ?
                _this.formatError('Reikšmė neegzistuoja sąraše') :
                undefined;
        });
        return this;
    };
    StringSchema.prototype.required = function () {
        var _this = this;
        this.addSyncRule(function (v) { return !isAbsent(v) && v.length > 0 ? undefined : _this.formatError('Reikšmė yra privaloma'); });
        return this;
    };
    return StringSchema;
}(BaseSchema));
export { StringSchema };
//# sourceMappingURL=string.js.map