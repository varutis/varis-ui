const fse = require('fs');

(async () => {
    try {
        fse.copyFile('./src/index.css', './dist/style.css', (err) => {
            if (err) {
                throw err;
            }
            console.log('index.css was copied to dist folder');
        });
    } catch (ex) {
        console.error('Error', ex);
    }
})();