import * as React from 'react';
import { SortableContainer, SortableContainerProps } from 'react-sortable-hoc';

export interface SortableListProps {
}

class SortableListComponent extends React.Component<SortableListProps> {
    render() {
        const { children } = this.props;
        return (
            <div>
                {children}
            </div>
        );
    }
}

export const SortableList: React.ComponentClass<SortableContainerProps & SortableListProps> = SortableContainer(SortableListComponent);