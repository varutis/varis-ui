import * as React from 'react';
import { SortableElement, SortableElementProps } from 'react-sortable-hoc';

export interface SortableListItemProps {
}

class SortableListItemComponent extends React.Component<SortableListItemProps> {
    render() {
        const { children } = this.props;
        return (
            <div>
                {children}
            </div>
        );
    }
}

export const SortableListItem: React.ComponentClass<SortableElementProps & SortableListItemProps> = SortableElement(SortableListItemComponent);