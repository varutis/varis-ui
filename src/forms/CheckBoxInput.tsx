import * as React from 'react';
import * as cn from 'classnames';
import { Tooltip } from 'react-tippy';
import formField from './formField';
import { nameToId } from '../utils';
import { FieldProps } from '.';

export type CheckBoxInputProps = {
    name: string;
    className?: string;
    description?: string;
    form?: string;
    text?: string;
    value?: boolean;
    disabled?: boolean;
    autoFocus?: boolean;
    autocomplete?: string;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
    style?: React.CSSProperties;
};

export class CheckBoxInput extends React.Component<CheckBoxInputProps> {
    static parse = (value: string | boolean) => !!value;
    render() {
        const { autoFocus, className, description, disabled, name, form, text, value, onBlur, onChange, onFocus, style, autocomplete } = this.props;
        return (
            <div className="d-flex flex-row">
                <div className={cn('form-check', className)} style={style != null ? style : {}}>
                    <label className="form-check-label">
                        <input
                            id={name}
                            type="checkbox"
                            className="form-check-input"
                            disabled={disabled}
                            autoFocus={autoFocus}
                            checked={!!value}
                            autoComplete={autocomplete}
                            onBlur={onBlur}
                            onChange={onChange}
                            onFocus={onFocus}
                        />
                        {text && ` ${text}`}
                        {description && <Tooltip
                            title={description}
                            position="top"
                            duration={0}
                            delay={[100, 0]}
                            arrow={true}
                            key={`tooltip_${nameToId(name)}`}
                        >
                            <i key="description" id={`${form}${form && '_'}${nameToId(name)}_descr`} className="ml-2 fas fa-info-circle" />
                        </Tooltip>}
                    </label>
                </div>
            </div>
        );
    }
}

export default CheckBoxInput;

export const CheckBoxField: React.ComponentClass<FieldProps & CheckBoxInputProps> = formField(CheckBoxInput);
