import * as React from 'react';
import * as ReactDatetime from 'react-datetime';
import { findDOMNode } from 'react-dom';
import { Moment } from 'moment';
import formField from './formField';
import Input from './Input';
import 'react-datetime/css/react-datetime.css';
import 'moment/locale/lt';
import { FieldProps } from '.';

export interface DateTimeInputProps {
    autoFocus?: boolean;
    className?: string;
    description?: string;
    disabled?: boolean;
    form?: string;
    name: string;
    placeholder?: string;
    showDate?: boolean | string;
    showTime?: boolean | string;
    showToggleButton?: boolean;
    value?: Moment;
    autoComplete?: string;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
}

export interface DateTimeInputState {
    isOpen: boolean;
}

export class DateTimeInput extends React.Component<DateTimeInputProps, DateTimeInputState> {
    private component: ReactDatetime;

    constructor(props: DateTimeInputProps) {
        super(props);
        this.state = {
            isOpen: false
        };
    }

    handleToggle = (e: React.MouseEvent<{}>) => {
        e.preventDefault();
        if (this.component) {
            const input = findDOMNode(this.component).querySelector('input');
            if (input) {
                input.focus();
                const isOpen = this.state.isOpen;
                this.setState({ isOpen: !isOpen });
            }
        }
    }

    handleOnChange = (e: any) => { // tslint:disable-line:no-any
        if (this.props.onChange) {
            this.props.onChange!(e);
            this.setState({ isOpen: false });
        }
    }

    handleOnBlur = (e: any) => { // tslint:disable-line:no-any
        if (this.props.onBlur) {
            this.props.onBlur!(e);
            this.setState({ isOpen: false });
        }
    }

    handleOnFocus = (e: any, showToggleButton: boolean) => { // tslint:disable-line:no-any
        if (this.props.onFocus) {
            this.props.onFocus!(e);
            if (showToggleButton) {
                this.setState({ isOpen: true });
            } else {
                this.setState({ isOpen: false });
            }
        }
    }

    handleOnClick = (e: any, showToggleButton: boolean) => { // tslint:disable-line:no-any
        if (!showToggleButton) {
            this.setState({ isOpen: false });
        }
    }

    toggleButton = (): JSX.Element => {
        return (
            <div key="toggle" className="input-group-append">
                <button className="btn btn-outline-secondary" type="button" onClick={this.handleToggle}>
                    <i className="fas fa-calendar"></i>
                </button>
            </div>
        );
    }

    render() {
        const props = {
            showDate: true,
            showTime: false,
            showToggleButton: true,
            ...this.props
        };

        const dateTimeProps: ReactDatetime.DatetimepickerProps = {
            inputProps: {
                autoFocus: props.autoFocus,
                className: 'form-control' + (props.className ? ` ${props.className}` : ''),
                disabled: props.disabled,
                name: props.name,
                placeholder: props.placeholder,
                autoComplete: props.autoComplete,
                onClick: (e: any) => { this.handleOnClick(e, props.showToggleButton); }, // tslint:disable-line no-any
                onFocus: (e: any) => { this.handleOnFocus(e, props.showToggleButton); } // tslint:disable-line no-any
            },
            open: this.state.isOpen,
            dateFormat: props.showDate,
            timeFormat: props.showTime,
            locale: 'lt',
            strictParsing: true,
            utc: false,
            value: props.value as any, // tslint:disable-line no-any
            onBlur: (e: any) => { this.handleOnBlur(e); }, // tslint:disable-line no-any
            onChange: (e: any) => { this.handleOnChange(e); }, // tslint:disable-line no-any
            onFocus: (e: any) => { this.handleOnFocus(e, props.showToggleButton); } // tslint:disable-line no-any
        };

        return (
            <Input
                name={props.name}
                form={props.form}
                component={(<ReactDatetime key="component" ref={(x: ReactDatetime) => this.component = x} {...dateTimeProps} />)}
                addons={props.showToggleButton ? [this.toggleButton()] : undefined}
                description={props.description}
            />
        );
    }
}

export default DateTimeInput;

export const DateTimeField: React.ComponentClass<FieldProps & DateTimeInputProps> = formField(DateTimeInput);
