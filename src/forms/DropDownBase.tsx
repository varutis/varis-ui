import * as React from 'react';
import * as cn from 'classnames';
import Input from './Input';
import { Option } from '../model';
import { nameToId } from '../utils';

export interface DropDownItem extends Option<any> { // tslint:disable-line no-any
}

export const messages = {
    loading: 'Siunčiami duomenys...',
    noItems: 'Įrašų nerasta'
};

export interface DropDownProps {
    name: string;
    form?: string;
    className?: string;
    disabled?: boolean;
    description?: string;
    placeholder?: string;
    addons?: React.ReactElement<{}>[];
    size?: 'sm' | 'lg';
    iconName: string;
    emptyMessage?: string;
    minSearchSymbols?: number;
    multiSection?: boolean;
    showGroupTitle?: boolean;
    focusOnItemClick?: boolean;
    loading?: boolean;
    items: DropDownItem[];
    showCodeWithName?: boolean;
    value: DropDownItem | null;
    onFocus?: React.FocusEventHandler<{}>;
    onBlur?: (newValue: DropDownItem | null) => void;
    onChange: (newValue: DropDownItem | null) => void;
    onOpen?: (text: string) => void;
    onSearch?: (text: string) => void;
}

export interface DropDownState {
    open: boolean;
    selectedIndex: number;
    text: string;
}

export class DropDownBase extends React.Component<DropDownProps, DropDownState> {
    static defaultProps: Partial<DropDownProps> = {
        emptyMessage: messages.noItems,
        showGroupTitle: true,
        focusOnItemClick: true
    };

    private itemJustSelected = false;
    private selectedValue: DropDownItem | null;
    private inputRef: HTMLInputElement | null;

    constructor(props: DropDownProps) {
        super(props);
        this.selectedValue = this.props.value;
        this.state = {
            open: false,
            selectedIndex: -1,
            text: this.selectedValue ?
                this.props.showCodeWithName === true ?
                    this.selectedValue.code != null ?
                        this.selectedValue.code
                        : ''
                    : this.selectedValue.name
                : ''
        };
    }

    componentWillReceiveProps(nextProps: DropDownProps) {
        if (nextProps.value !== this.selectedValue) {
            let textCode = nextProps.value ? nextProps.value.code : '';
            let textName = nextProps.value ? nextProps.value.name : '';
            const text = nextProps.showCodeWithName === true ? (textCode != null ? textCode : '') : textName;
            this.selectedValue = nextProps.value;
            this.setState({ text });
        }
    }

    open() {
        if (this.props.disabled) {
            return;
        }

        if (this.props.onOpen) {
            this.props.onOpen(this.state.text);
        }

        this.setState({ open: true, selectedIndex: -1 });
    }

    search(text: string) {
        if (this.props.disabled) {
            return;
        }

        if (text !== this.state.text) {
            this.setState({ text });
        }

        if (this.props.onSearch) {
            this.props.onSearch(text);
        }

        this.setState({ open: true, selectedIndex: -1 });
    }

    close() {
        const text = this.selectedValue != null ? this.selectedValue.name : '';
        const textCode = this.selectedValue != null ? this.selectedValue.code != null ? this.selectedValue.code : '' : '';
        this.setState({ open: false, text: this.props.showCodeWithName === true ? textCode : text });
    }

    focus() {
        if (this.inputRef) {
            const inputRef = this.inputRef;
            setTimeout(() => { inputRef.focus(); inputRef.select(); }, 0);
        }
    }

    getMinSearchSymbols() {
        return this.props.minSearchSymbols !== undefined ? this.props.minSearchSymbols : 3;
    }

    handleFocus = (e: React.FocusEvent<{}>) => {
        if (this.getMinSearchSymbols() === 0 && this.props.onSearch) {
            this.props.onSearch('');
        }

        if (!this.itemJustSelected) {
            this.open();
        }

        this.itemJustSelected = false;
        if (this.props.onFocus) {
            this.props.onFocus(e);
        }
    }

    handleBlur = () => {
        this.close();
        if (this.props.onBlur) {
            this.props.onBlur(this.selectedValue);
        }
    }

    handleClick = () => {
        this.open();
    }

    handleKeyDown = (e: React.KeyboardEvent<{}>) => {
        if (e.key === 'ArrowDown') {
            this.processArrowDown(e);
        } else if (e.key === 'ArrowUp') {
            this.processArrowUp(e);
        } else if (e.key === 'Enter') {
            this.processEnter(e);
        } else if (e.key === 'Escape') {
            this.processEscape(e);
        }
    }

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (this.selectedValue != null) {
            this.selectedValue = null;
            this.props.onChange(this.selectedValue);
        }

        this.search(e.target.value);
    }

    handleItemClick = (e: React.MouseEvent<{}>, item: DropDownItem) => {
        if (item !== this.selectedValue) {
            this.props.onChange(item);
        }

        this.close();
        if (this.props.focusOnItemClick) {
            this.itemJustSelected = true;
            this.focus();
        }
    }

    handleIconClick = (e: React.MouseEvent<{}>) => {
        this.focus();
    }

    processArrowDown(e: React.KeyboardEvent<{}>) {
        e.preventDefault();
        const { open, selectedIndex } = this.state;
        if (!open) {
            this.open();
        } else if (selectedIndex < this.props.items.length - 1) {
            this.setState({ selectedIndex: selectedIndex + 1 });
        }
    }

    processArrowUp(e: React.KeyboardEvent<{}>) {
        e.preventDefault();
        const { open, selectedIndex } = this.state;
        if (open && selectedIndex > 0) {
            this.setState({ selectedIndex: selectedIndex - 1 });
        }
    }

    processEnter(e: React.KeyboardEvent<{}>) {
        if (this.state.open) {
            e.preventDefault();
            const { items } = this.props;
            const { selectedIndex } = this.state;
            if (selectedIndex >= 0 && selectedIndex < items.length) {
                const value = items[selectedIndex];
                if (value !== this.selectedValue) {
                    this.props.onChange(value);
                }
            }

            this.close();
        }
    }

    processEscape(e: React.KeyboardEvent<{}>) {
        e.preventDefault();
        if (this.state.open) {
            this.close();
        }
    }

    render() {
        return (
            <div className="dropdown">
                {this.renderInputGroup()}
                {this.renderIcon()}
                {this.renderMenu()}
            </div>
        );
    }

    renderInputGroup() {
        return (
            <Input
                name={this.props.name}
                form={this.props.form}
                description={this.props.description}
                component={this.renderInput()}
                addons={this.props.addons}
                size={this.props.size}
            />
        );
    }

    renderInput() {
        const { name, form, className, disabled, placeholder, description, addons, size } = this.props;
        const { text } = this.state;
        const hasInputGroup = description || (addons && addons.length > 0);
        const inputSize = !hasInputGroup && (
            size === 'sm' ? 'form-control-sm' : size === 'lg' ? 'form-control-lg' : undefined
        );
        return (
            <input
                id={nameToId(name, form)}
                key="component"
                ref={x => this.inputRef = x}
                type="text"
                className={cn('form-control form-control-with-icon', className, inputSize)}
                disabled={disabled}
                placeholder={placeholder}
                autoComplete="off"
                value={text}
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
                onClick={this.handleClick}
                onKeyDown={this.handleKeyDown}
                onChange={this.handleChange}
            />
        );
    }

    renderIcon() {
        const { loading, iconName } = this.props;
        return (
            <div className="form-control-icon" onClick={this.handleIconClick}>
                <i className={cn('fas fa-fw', loading ? 'fa-spinner fa-pulse' : iconName)}></i>
            </div>
        );
    }

    menuContent = () => {
        const { items, loading, emptyMessage, value } = this.props;
        const { text } = this.state;
        return (
            <span>
                {this.renderMenuItems()}
                {items.length === 0 && <div className="dropdown-item disabled">{
                    loading ? messages.loading :
                        text === null || text.length < this.getMinSearchSymbols() || value != null ? emptyMessage : messages.noItems
                }</div>}
            </span>
        );
    }

    renderMenu() {
        const { open } = this.state;
        const { items } = this.props;
        if (items.length > 15) {
            return (
                <div className={cn('dropdown-menu', { show: open })} style={{ overflowY: 'scroll', height: 410 }}>
                    {this.menuContent()}
                </div >
            );
        }
        return (
            <div className={cn('dropdown-menu', { show: open })}>
                {this.menuContent()}
            </div >
        );
    }

    renderMenuItems() {
        const { items, multiSection, showGroupTitle, showCodeWithName } = this.props;
        if (items.length === 0) {
            return [];
        }

        if (!multiSection) {
            return items.map((item, index) => this.renderMenuItem(item, index, showCodeWithName != null ? showCodeWithName : false));
        }

        let currentGroup: string | null = '$$initial';
        return items.reduce((menu, item, index) => {
            if (item.group !== currentGroup) {
                if (index > 0) {
                    menu.push(<div key={`d${index}`} className="dropdown-divider"></div>);
                }

                if (showGroupTitle) {
                    menu.push(<h6 key={`h${index}`} className="dropdown-header">{item.group}</h6>);
                }

                currentGroup = item.group ? item.group : null;
            }

            menu.push(this.renderMenuItem(item, index, showCodeWithName != null ? showCodeWithName : false));

            return menu;
        }, [] as React.ReactNode[]);
    }

    renderMenuItem(item: DropDownItem, index: number, showCodeWithName: boolean) {
        const { selectedIndex } = this.state;
        return (
            <button
                key={index}
                type="button"
                className={cn('dropdown-item', index === selectedIndex && 'active')}
                tabIndex={-1}
                onMouseDown={e => this.handleItemClick(e, item)}
            >
                {(showCodeWithName != null && showCodeWithName === true ? '(' + item.code + ') ' : '') + item.name}
            </button>
        );
    }
}
