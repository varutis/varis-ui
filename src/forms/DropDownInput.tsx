import * as React from 'react';
import formField from './formField';
import { DropDownBase, DropDownItem } from './DropDownBase';
import { FieldProps } from '.';

export interface DropDownInputProps {
    name: string;
    form?: string;
    className?: string;
    disabled?: boolean;
    description?: string;
    placeholder?: string;
    multiSection?: boolean;
    showGroupTitle?: boolean;
    focusOnItemClick?: boolean;
    addons?: React.ReactElement<{}>[];
    size?: 'sm' | 'lg';
    options: DropDownItem[];
    showCodeWithName?: boolean;
    value?: number | string | null;
    onFocus?: React.FocusEventHandler<{}>;
    onBlur?: (e: {}, newValue: any) => void; // tslint:disable-line no-any
    onChange?: (e: {}, newValue: any) => void; // tslint:disable-line no-any
}

export interface DropDownInputState {
    items: DropDownItem[];
}

export class DropDownInput extends React.Component<DropDownInputProps, DropDownInputState> {
    private component: DropDownBase | null;
    private selectedValue: DropDownItem | null;

    constructor(props: DropDownInputProps) {
        super(props);
        this.selectedValue = this.format(this.props.value);
        this.state = {
            items: []
        };
    }

    componentWillReceiveProps(nextProps: DropDownInputProps) {
        this.selectedValue = this.format(nextProps.value);
    }

    componentDidMount() {
        this.validateValue(this.selectedValue);
    }

    focus() {
        if (this.component) {
            this.component.focus();
        }
    }

    parse(value: DropDownItem | null) {
        return value ? value.id : null;
    }

    format(value: number | string | null | undefined) {
        if (value == null || value === '') {
            return null;
        }
        const option = this.props.options.find(x => x.id === value);
        if (!option) {
            return { id: '-999999999', name: '', code: null, type: null, group: null } as DropDownItem;
        }

        return option;
    }

    datasource = (query: string) => {
        if (!query || this.props.options.length === 0) {
            return this.props.options;
        }

        const q = query.toLowerCase();
        return this.props.options.filter(x => x.name.substr(0, q.length).toLowerCase() === q);
    }

    isUnknownValue(option: DropDownItem | null) {
        return option && option.id === '-999999999';
    }

    validateValue(selectedValue: DropDownItem | null) {
        if (this.isUnknownValue(selectedValue)) {
            this.handleChange(selectedValue);
            this.handleBlur(selectedValue);
        }
    }

    handleBlur = (newValue: DropDownItem | null) => {
        this.selectedValue = this.parse(newValue);
        if (this.props.onBlur) {
            this.props.onBlur(this.selectedValue as any, undefined); // tslint:disable-line no-any
        }
    }

    handleChange = (newValue: DropDownItem | null) => {
        this.selectedValue = this.parse(newValue);
        if (this.props.onChange) {
            this.props.onChange(this.selectedValue as any, undefined); // tslint:disable-line no-any
        }
    }

    handleOpen = (text: string) => {
        this.setState({ items: this.props.options });
    }

    handleSearch = (text: string) => {
        if (text === '') {
            this.setState({ items: this.props.options });
            return;
        }

        const normalizedText = text.toLowerCase();
        const items = this.props.options.filter(item => item.name.substr(0, normalizedText.length).toLowerCase() === normalizedText);
        this.setState({ items });
    }

    render() {
        const {
            className,
            disabled,
            name,
            form,
            description,
            placeholder,
            multiSection,
            showGroupTitle,
            addons,
            size,
            focusOnItemClick,
            onFocus,
            showCodeWithName
        } = this.props;
        return (
            <DropDownBase
                ref={x => this.component = x}
                name={name}
                form={form}
                className={className}
                disabled={disabled}
                description={description}
                placeholder={placeholder}
                multiSection={multiSection}
                showGroupTitle={showGroupTitle}
                addons={addons}
                size={size}
                iconName="fa-caret-down"
                focusOnItemClick={focusOnItemClick}
                items={this.state.items}
                showCodeWithName={showCodeWithName}
                value={this.selectedValue}
                onFocus={onFocus}
                onBlur={this.handleBlur}
                onChange={this.handleChange}
                onOpen={this.handleOpen}
                onSearch={this.handleSearch}
            />
        );
    }
}

export default DropDownInput;

export const DropDownField: React.ComponentClass<FieldProps & DropDownInputProps> = formField(DropDownInput);
