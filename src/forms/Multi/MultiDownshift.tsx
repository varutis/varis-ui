import * as React from 'react';
const debounce = require('lodash.debounce');
import Downshift, { GetItemPropsOptions, getToggleButtonPropsOptions } from 'downshift';
import AutosizeInput from 'react-input-autosize';
import { TagValue, TagModel } from './TagValue';
// import matchSorter from 'match-sorter';
import formField from '../formField';
import { FieldProps } from '..';
import Input from '../Input';
import { ButtonStyle, MenuStyle, MenuItemStyle, WrapperStyle, InputStyle } from './Styles';
import { DropDownItem } from '../DropDownBase';

export interface MultiDownshiftProps {
    name: string;
    form?: string;
    description?: string;
    value?: DropDownItem[] | null;
    onChange?: (e: {}, newValue: any) => void; // tslint:disable-line no-any
    onSearch: (text: string) => Promise<DropDownItem[]>;
}

export interface MultiDownshiftState {
    inputValue: string;
    loading: boolean;
    items: DropDownItem[];
    isOpen: boolean;
}

export interface MultiDownshiftCancelationToken {
    canceled: boolean;
    cancel(): void;
}

function createCancelationToken(): MultiDownshiftCancelationToken {
    return {
        canceled: false,
        cancel() {
            this.canceled = true;
        }
    };
}

export class MultiDownshift extends React.Component<MultiDownshiftProps, MultiDownshiftState> {
    private inputRef: HTMLInputElement | null;
    private inputWrapperRef: HTMLDivElement | null;
    private selectedItems: DropDownItem[];
    private cancelationToken = createCancelationToken();

    constructor(props: MultiDownshiftProps) {
        super(props);
        this.selectedItems = this.format(this.props.value);
        this.state = {
            inputValue: '',
            loading: false,
            items: [],
            isOpen: false
        };
    }

    componentWillReceiveProps(nextProps: MultiDownshiftProps) {
        this.selectedItems = this.format(nextProps.value);
    }

    format(value: DropDownItem[] | null | undefined) {
        if (!value) {
            return [] as DropDownItem[];
        }

        return value;
    }

    /// DOWNSHIFT:

    handleStateChange = (changes: any, downshiftState: any) => { // tslint:disable-line:no-any
        if (!downshiftState.isOpen) {
            this.setState({ inputValue: '' });
        }

        if (changes.hasOwnProperty('inputValue') && typeof changes.inputValue === 'string') {
            var inputValue = changes.inputValue as string;
            this.handleSearch(inputValue);
            this.setState({ isOpen: true });
        }
    }

    handleOnSelect = (selectedItem: DropDownItem) => {
        this.selectedItems = [...this.selectedItems, selectedItem];
        if (this.props.onChange) {
            this.props.onChange(this.selectedItems as any, undefined); // tslint:disable-line no-any
            this.setState({ isOpen: false });
        }
    }

    handleItemToString = (i: DropDownItem) => {
        return i ? i.name : '';
    }

    /// WRAPPER:

    onWrapperClick = (e: React.MouseEvent<{}>) => {
        if (this.inputWrapperRef === e.target || this.inputRef === e.target) {
            this.focusOnInput();
            e.stopPropagation();
            e.preventDefault();
        }
    }

    focusOnInput() {
        if (this.inputRef) {
            const inputRef = this.inputRef;
            setTimeout(() => { inputRef.focus(); inputRef.select(); }, 0);

            this.setState({ isOpen: true });
            this.handleSearch(this.state.inputValue);
        }
    }

    /// SEARCH

    // tslint:disable-next-line member-ordering
    handleSearch = debounce((text: string) => {
        this.setState({ isOpen: true });

        this.cancelationToken.cancel();
        this.cancelationToken = createCancelationToken();
        this.processSearch(text, this.cancelationToken);
    }, 200);

    async processSearch(text: string, cancelationToken: MultiDownshiftCancelationToken) {
        this.setState({ loading: true, items: [] });
        try {
            const items = await this.props.onSearch(text);
            if (!cancelationToken.canceled) {
                this.setState({ loading: false, items });
            }
        } catch (ex) {
            if (!cancelationToken.canceled) {
                this.setState({ loading: false });
            }
        }
    }

    /// TAG:

    handleOnTagBlur = (e: {}, tag: TagModel) => {
        const copy = [...this.selectedItems];
        copy.splice(tag.index, 1, tag.value);
        this.selectedItems = copy;

        if (this.props.onChange) {
            this.props.onChange(this.selectedItems as any, undefined); // tslint:disable-line no-any
        }
    }

    handleOnTagRemove = (tag: TagModel) => {
        const copy = [...this.selectedItems];
        copy.splice(tag.index, 1);
        this.selectedItems = copy;

        if (this.props.onChange) {
            this.props.onChange(this.selectedItems as any, undefined); // tslint:disable-line no-any
        }
    }

    /// INPUT:

    handleOnInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ inputValue: e.target.value });
    }

    handleOnInputKeyDown = (e: React.KeyboardEvent<{}>) => {
        switch (e.keyCode) {
            case 8: // backspace
            case 46: // backspace
                if (!this.state.inputValue) {
                    e.preventDefault();
                    this.handleOnTagRemove({ value: this.selectedItems[this.selectedItems.length - 1], index: this.selectedItems.length - 1 });
                }
                return;
            default:
                return;
        }
    }

    renderButton(
        isOpen: boolean,
        getToggleButtonProps: (options?: getToggleButtonPropsOptions | undefined) => any // tslint:disable-line:no-any
    ) {
        return (
            <button
                {...getToggleButtonProps()}
                style={ButtonStyle}
                onClick={() => {
                        this.setState({ isOpen: !this.state.isOpen });
                        this.handleSearch(this.state.inputValue);
                    }
                }
            >
                <i className={'fas fa-search'}></i>
            </button>
        );
    }

    renderMenu(
        items: DropDownItem[],
        getItemProps: (options: GetItemPropsOptions<any>) => any, // tslint:disable-line:no-any
        highlightedIndex: number | null,
        selectedItem: DropDownItem
    ) {
        return (
            <div style={MenuStyle}>
                {items.map((item: DropDownItem, index: number) => (
                    this.renderMenuItem(item, index, getItemProps, highlightedIndex, selectedItem)
                ))}
            </div>
        );
    }

    renderMenuItem(
        item: DropDownItem,
        index: number,
        getItemProps: (options: GetItemPropsOptions<any>) => any, // tslint:disable-line:no-any
        highlightedIndex: number | null,
        selectedItem: DropDownItem
    ) {
        return (
            <div
                key={`item-${index}`}
                {...getItemProps({
                    item,
                    index
                })}
                style={MenuItemStyle(item, index, highlightedIndex, selectedItem)}
            >
                {item.name}
            </div>
        );
    }

    render() {
        const { onChange, onSearch, ...rest } = this.props;
        const { items } = this.state;
        return (
            <Downshift
                onStateChange={this.handleStateChange}
                itemToString={this.handleItemToString}
                onSelect={this.handleOnSelect}
                selectedItem={this.selectedItems}
                isOpen={this.state.isOpen}
                onOuterClick={() => this.setState({ isOpen: false })}
                {...rest}
            >
                {({
                    getInputProps,
                    getToggleButtonProps,
                    getItemProps,
                    isOpen,
                    selectedItem,
                    highlightedIndex
                }) => {
                    const _inputProps = getInputProps({
                        value: this.state.inputValue,
                        onChange: this.handleOnInputChange,
                        onKeyDown: this.handleOnInputKeyDown
                    });
                    const tagItems = selectedItem.map((item: string, index: number) => ({ value: item, index }));
                    return (
                        <div>
                            <div style={{ position: 'relative' }}>
                                <div
                                    ref={(x: HTMLDivElement) => this.inputWrapperRef = x}
                                    onClick={this.onWrapperClick}
                                    tabIndex={-1}
                                    style={WrapperStyle}
                                >
                                    {tagItems.map((tag: TagModel) => (
                                        <TagValue
                                            key={`Tag-${tag.index}`}
                                            onBlur={this.handleOnTagBlur}
                                            onRemove={this.handleOnTagRemove}
                                            tag={tag}
                                        />
                                    ))}
                                    <Input
                                        name={this.props.name}
                                        form={this.props.form}
                                        component={(
                                            <AutosizeInput
                                                {..._inputProps}
                                                id={this.props.name}
                                                ref={(x: HTMLInputElement) => this.inputRef = x}
                                                inputStyle={InputStyle}
                                            />
                                        )}
                                        description={this.props.description}
                                    />

                                </div>

                                {this.renderButton(isOpen, getToggleButtonProps)}
                            </div>
                            {!isOpen ? null : (this.renderMenu(items, getItemProps, highlightedIndex, selectedItem))}
                        </div>
                    );
                }}
            </Downshift>
        );
    }
}

export default MultiDownshift;

export const MultiDownshiftField: React.ComponentClass<FieldProps & MultiDownshiftProps> = formField(MultiDownshift);