import { CSSWideKeyword } from 'react';
import { DropDownItem } from '../DropDownBase';

export const InputStyle = {
    border: 'none',
    outline: 'none',
    cursor: 'inherit',
    backgroundColor: 'transparent',
    fontSize: '14px'
};

export const WrapperStyle = {
    display: 'flex',
    flexDirection: 'row' as CSSWideKeyword | 'row' | 'row-reverse' | 'column' | 'column-reverse',
    flexWrap: 'wrap' as CSSWideKeyword | 'nowrap' | 'wrap' | 'wrap-reverse',
    alignItems: 'center' as CSSWideKeyword | 'flex-start' | 'flex-end' | 'center' | 'baseline' | 'stretch',
    flex: '1 1 auto',
    padding: '4px',
    border: '1px #c5c4c4 solid',
    borderRadius: '4px'
};

export const ButtonStyle = {
    backgroundColor: 'transparent',
    border: 'none',
    position: 'absolute',
    right: 0,
    top: 0,
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
};

export const MenuStyle = {
    position: 'absolute' as CSSWideKeyword | 'static' | 'relative' | 'absolute' | 'fixed' | 'sticky',
    zIndex: 1000,
    minWidth: '94.5%',
    backgroundColor: 'white',
    maxHeight: '20rem',
    overflowY: 'auto' as CSSWideKeyword | 'auto' | 'hidden' | 'scroll' | 'visible',
    overflowX: 'hidden' as CSSWideKeyword | 'auto' | 'hidden' | 'scroll' | 'visible',
    borderTopWidth: '0',
    outline: '0',
    borderRadius: '0 0 .28571429rem .28571429rem',
    transition: 'opacity .1s ease',
    boxShadow: '0 2px 3px 0 rgba(34,36,38,.15)',
    borderColor: '#96c8da',
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderStyle: 'solid'
};

export const MenuItemStyle = (
    item: DropDownItem,
    index: number,
    highlightedIndex: number | null,
    selectedItem: DropDownItem
) => {
    return {
        position: 'relative',
        cursor: 'pointer',
        display: 'block',
        border: 'none',
        height: 'auto',
        textAlign: 'left',
        borderTop: 'none',
        lineHeight: '1em',
        fontSize: '1rem',
        textTransform: 'none',
        boxShadow: 'none',
        padding: '.8rem 1.1rem',
        whiteSpace: 'normal',
        wordWrap: 'normal',
        color: highlightedIndex === index || selectedItem === item ?
            'rgba(0,0,0,.95)' : 'rgba(0,0,0,.87)',
        background: highlightedIndex === index ? 'rgba(0,0,0,.03)' : '',
        fontWeight: selectedItem === item ? '700' : '400'
    };
};