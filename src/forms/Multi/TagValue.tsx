import * as React from 'react';
import { DropDownItem } from '../DropDownBase';

export interface TagModel {
    value: DropDownItem;
    index: number;
}

export interface TagValueProps {
    tag: TagModel;
    onRemove: (tag: TagModel) => void;
    onBlur: (e: {}, tag: TagModel) => void;
}

export interface TagValueState {
    value: DropDownItem;
}

export class TagValue extends React.Component<TagValueProps, TagValueState> {

    constructor(props: TagValueProps) {
        super(props);

        this.state = {
            value: props.tag.value
        };
    }

    componentWillReceiveProps(nextProps: TagValueProps) {
        if (nextProps.tag !== this.props.tag) {
            this.setState({ value: nextProps.tag.value });
        }
    }

    onBlur = (e: {}) => {
        const { onBlur, tag } = this.props;
        if (onBlur) {
            onBlur(e, { ...tag, value: this.state.value });
        }
    }

    onRemove = (e: React.MouseEvent<{}>) => {
        const { onRemove, tag } = this.props;

        e.preventDefault();
        e.stopPropagation();
        if (onRemove) {
            onRemove({ ...tag, value: this.state.value });
        }
    }

    render() {
        const { value } = this.state;

        return (
            <div style={{ marginRight: 4, backgroundColor: '#ccc', padding: 2 }}>
                {value.name}{' '}<span style={{ cursor: 'pointer' }} onClick={this.onRemove}>
                    <i className="fas fa-times"></i>
                </span>
            </div>
        );
    }
}
