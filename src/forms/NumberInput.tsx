import { TextInput, TextInputProps } from './TextInput';
import formField, { FormFieldOptions } from './formField';
import { FieldProps } from '.';

const numberFieldOptions: FormFieldOptions = {
    format(value: number | string | null | undefined) {
        return value == null ? '' : typeof (value) === 'number' ? value.toString().replace('.', ',') : value;
    },
    normalize(value: number | string | null | undefined) {
        return typeof (value) === 'string' && value.indexOf('.') > 0 ? value.replace('.', ',') : value;
    }
};

export const NumberField: React.ComponentClass<FieldProps & TextInputProps> = formField(TextInput, numberFieldOptions);
