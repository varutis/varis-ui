import * as React from 'react';
import formField from './formField';
import Input from './Input';
import { FieldProps } from '.';

export interface PasswordInputProps {
    name: string;
    addon?: React.ReactElement<{}>;
    autoComplete?: string;
    autoFocus?: boolean;
    className?: string;
    description?: string;
    disabled?: boolean;
    form?: string;
    maxLength?: number;
    placeholder?: string;
    value?: string | number;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
}

export class PasswordInput extends React.Component<PasswordInputProps> {
    render() {
        return (
            <Input
                name={this.props.name}
                form={this.props.form}
                component={(
                    <input
                        key="component"
                        id={this.props.name}
                        autoComplete={this.props.autoComplete}
                        autoFocus={this.props.autoFocus}
                        className={'form-control' + (this.props.className ? ` ${this.props.className}` : '')}
                        disabled={this.props.disabled}
                        maxLength={this.props.maxLength}
                        placeholder={this.props.placeholder}
                        type="password"
                        value={this.props.value}
                        onBlur={this.props.onBlur}
                        onChange={this.props.onChange}
                        onFocus={this.props.onFocus}
                    />
                )}
                addons={this.props.addon ? [this.props.addon] : undefined}
                description={this.props.description}
            />
        );
    }
}

export default PasswordInput;

export const PasswordField: React.ComponentClass<FieldProps & PasswordInputProps> = formField(PasswordInput);
