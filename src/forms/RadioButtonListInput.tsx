import * as React from 'react';
import * as cn from 'classnames';
import { Tooltip } from 'react-tippy';
import formField from './formField';
import { OptionWithDescription, ValueEvent } from '../model';
import { nameToId } from '../utils';
import { FieldProps } from '.';

export interface RadioButtonListInputProps {
    name: string;
    options: OptionWithDescription<number | string>[];
    className?: string;
    description?: string;
    disabled?: boolean;
    form?: string;
    value?: number | string;
    autoComplete?: string;
    onBlur?: ValueEvent;
    onFocus?: React.FocusEventHandler<{}>;
    onChange?: ValueEvent;
    inlineForm?: boolean;
}

export class RadioButtonListInput extends React.Component<RadioButtonListInputProps> {
    handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
        if (this.props.onBlur) {
            this.props.onBlur(e.currentTarget.value);
        }
    }

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (this.props.onChange && e.target.checked) {
            this.props.onChange(e.target.value);
        }
    }

    render() {
        const { className, description, disabled, name, form, options, value, inlineForm, onFocus, autoComplete } = this.props;
        const inline = inlineForm === undefined ? true : inlineForm;
        return (
            <div>
                {options.map(x => (
                    <div key={x.id} className={cn('form-check', inline && 'form-check-inline', className)}>
                        <label className="form-check-label">
                            <input
                                type="radio"
                                autoComplete={autoComplete}
                                name={name}
                                className="form-check-input"
                                disabled={disabled}
                                value={x.id}
                                checked={x.id === value || x.id.toString() === value}
                                onBlur={this.handleBlur}
                                onChange={this.handleChange}
                                onFocus={onFocus}
                            />
                            {x.name}
                            {x.description && <Tooltip
                                title={x.description}
                                position="top"
                                duration={0}
                                delay={[100, 0]}
                                arrow={true}
                                key={`item_tooltip_${nameToId(name)}`}
                            >
                                <i
                                    key="itemDescription"
                                    id={`${form}${form && '_'}${nameToId(name)}_item_descr`}
                                    className="ml-2 fas fa-info-circle"
                                />
                            </Tooltip>}
                        </label>
                    </div>
                ))}
                {description && <Tooltip
                    title={description}
                    position="top"
                    duration={0}
                    delay={[100, 0]}
                    arrow={true}
                    key={`tooltip_${nameToId(name)}`}
                >
                    <i key="description" id={`${form}${form && '_'}${nameToId(name)}_descr`} className="ml-2 fas fa-info-circle" />
                </Tooltip>}
            </div>
        );
    }
}

export default RadioButtonListInput;

export const RadioButtonListField: React.ComponentClass<FieldProps & RadioButtonListInputProps> = formField(RadioButtonListInput);
