import * as React from 'react';

export interface RichTextDisplayProps {
    value: string;
    plain?: boolean;
}

export const RichTextDisplay = ({ value, plain }: RichTextDisplayProps) => {
    if (!value) {
        return null;
    }

    return (
        <div dangerouslySetInnerHTML={{ __html: value }} />
    );
};

export default RichTextDisplay;
