import * as React from 'react';
import formField from './formField';
import Input from './Input';
import { FieldProps } from '.';

export interface TextInputProps {
    name: string;
    addon?: React.ReactElement<{}>;
    autoComplete?: string;
    autoFocus?: boolean;
    className?: string;
    description?: string;
    disabled?: boolean;
    readonly?: boolean;
    form?: string;
    maxLength?: number;
    placeholder?: string;
    value?: string | number;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
}

export class TextInput extends React.Component<TextInputProps> {
    render() {
        return (
            <Input
                name={this.props.name}
                form={this.props.form}
                component={(
                    <input
                        key="component"
                        id={this.props.name}
                        autoFocus={this.props.autoFocus}
                        autoComplete={this.props.autoComplete}
                        className={'form-control' + (this.props.className ? ` ${this.props.className}` : '')}
                        disabled={this.props.disabled}
                        readOnly={this.props.readonly}
                        maxLength={this.props.maxLength}
                        placeholder={this.props.placeholder}
                        type="text"
                        value={this.props.value}
                        onBlur={this.props.onBlur}
                        onChange={this.props.onChange}
                        onFocus={this.props.onFocus}
                    />
                )}
                addons={this.props.addon ? [this.props.addon] : undefined}
                description={this.props.description}
            />
        );
    }
}

export default TextInput;

export const TextField: React.ComponentClass<FieldProps & TextInputProps> = formField(TextInput);
