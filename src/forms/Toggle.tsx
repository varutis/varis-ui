import * as React from 'react';
import { Tooltip } from 'react-tippy';
import formField from './formField';
import { nameToId } from '../utils';
import { FieldProps } from '.';

export type ToggleProps = {
    name: string;
    className?: string;
    description?: string;
    form?: string;
    value?: boolean;
    disabled?: boolean;
    autoFocus?: boolean;
    autoComplete?: string;
    onBlur?: React.FocusEventHandler<{}>;
    onChange?: React.ChangeEventHandler<{}>;
    onFocus?: React.FocusEventHandler<{}>;
};

export class Toggle extends React.Component<ToggleProps> {
    static parse = (value: string | boolean) => !!value;
    render() {
        const { autoFocus, className, description, disabled, name, form, value, onBlur, onChange, onFocus, autoComplete } = this.props;
        return (
            <span className={className}>
                <label className="switch">
                    <input
                        id={name}
                        autoComplete={autoComplete}
                        type="checkbox"
                        className="form-check-input"
                        disabled={disabled}
                        autoFocus={autoFocus}
                        checked={!!value}
                        onBlur={onBlur}
                        onChange={onChange}
                        onFocus={onFocus}
                    />
                    <span className="slider round pt-3" />
                </label>
                {description && <Tooltip
                    title={description}
                    position="top"
                    duration={0}
                    delay={[100, 0]}
                    arrow={true}
                    key={`tooltip_${nameToId(name)}`}
                >
                    <i key="description" id={`${form}${form && '_'}${nameToId(name)}_descr`} className="ml-2 align-top fas fa-info-circle" />
                </Tooltip>}
            </span>
        );
    }
}

export default Toggle;

export const ToggleField: React.ComponentClass<FieldProps & ToggleProps> = formField(Toggle);
