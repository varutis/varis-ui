import * as React from 'react';
const debounce = require('lodash.debounce');
import { DropDownBase, DropDownItem } from './DropDownBase';
import formField from './formField';
import { FieldProps } from '.';

export interface TypeaheadInputProps {
    name: string;
    form?: string;
    className?: string;
    disabled?: boolean;
    description?: string;
    placeholder?: string;
    multiSection?: boolean;
    showGroupTitle?: boolean;
    focusOnItemClick?: boolean;
    addons?: React.ReactElement<{}>[];
    size?: 'sm' | 'lg';
    value?: DropDownItem | null;
    onFocus?: React.FocusEventHandler<{}>;
    minSearchSymbols?: number;
    onBlur?: (e: {}, newValue: any) => void; // tslint:disable-line no-any
    onChange?: (e: {}, newValue: any) => void; // tslint:disable-line no-any
    onSearch: (text: string) => Promise<DropDownItem[]>;
}

export interface TypeaheadInputState {
    loading: boolean;
    items: DropDownItem[];
}

export interface TypeaheadCancelationToken {
    canceled: boolean;
    cancel(): void;
}

function createCancelationToken(): TypeaheadCancelationToken {
    return {
        canceled: false,
        cancel() {
            this.canceled = true;
        }
    };
}

export class TypeaheadInput extends React.Component<TypeaheadInputProps, TypeaheadInputState> {
    private component: DropDownBase | null;
    private selectedValue: DropDownItem | null;
    private cancelationToken = createCancelationToken();

    constructor(props: TypeaheadInputProps) {
        super(props);
        this.selectedValue = this.format(this.props.value);
        this.state = {
            loading: false,
            items: []
        };
    }

    componentWillReceiveProps(nextProps: TypeaheadInputProps) {
        this.selectedValue = this.format(nextProps.value);
    }

    focus() {
        if (this.component) {
            this.component.focus();
        }
    }

    format(value: DropDownItem | '' | null | undefined) {
        if (value == null || value === '') {
            return null;
        }

        return value;
    }

    getMinSearchSymbols() {
        return (this.props.minSearchSymbols !== undefined) ? this.props.minSearchSymbols : 3;
    }

    getEmptyMessage() {
        const value = this.getMinSearchSymbols();
        if ((value > 9 && value < 20 ) || value % 10 === 0 || ((value % 100 > 9) && (value % 100 < 20))) {
            return `Įveskite ${value} simbolių`;
        }
        if (value === 1 || value !== 11 && (value % 100 !== 11 && value % 10 === 1)) {
            return `Įveskite ${value} simbolį`;
        }
        if (value > 1 && value < 10 || value > 21 && (value % 10 > 1 && value % 10 < 10)) {
            return `Įveskite ${value} simbolius`;
        }

        return '';
    }

    handleBlur = (newValue: DropDownItem | null) => {
        this.selectedValue = newValue;
        if (this.props.onBlur) {
            this.props.onBlur(this.selectedValue as any, undefined); // tslint:disable-line no-any
        }
    }

    handleChange = (newValue: DropDownItem | null) => {
        this.selectedValue = newValue;
        if (this.props.onChange) {
            this.props.onChange(this.selectedValue as any, undefined); // tslint:disable-line no-any
        }
    }

    // tslint:disable-next-line member-ordering
    handleSearch = debounce((text: string) => {
        if (text.length < this.getMinSearchSymbols()) { // TODO SMTH
            this.setState({ items: [] });
            return;
        }

        this.cancelationToken.cancel();
        this.cancelationToken = createCancelationToken();
        this.processSearch(text, this.cancelationToken);
    }, 200);

    async processSearch(text: string, cancelationToken: TypeaheadCancelationToken) {
        this.setState({ loading: true, items: [] });
        try {
            const items = await this.props.onSearch(text);
            if (!cancelationToken.canceled) {
                this.setState({ loading: false, items });
            }
        } catch (ex) {
            console.error(`Error in ${this.props.name} datasource`, ex); // tslint:disable-line no-console
            if (!cancelationToken.canceled) {
                this.setState({ loading: false });
            }
        }
    }

    render() {
        const {
            className,
            disabled,
            name,
            form,
            description,
            placeholder,
            multiSection,
            showGroupTitle,
            addons,
            size,
            focusOnItemClick,
            onFocus
        } = this.props;
        const { loading, items } = this.state;

        return (
            <DropDownBase
                ref={x => this.component = x}
                name={name}
                form={form}
                className={className}
                disabled={disabled}
                description={description}
                placeholder={placeholder}
                multiSection={multiSection}
                showGroupTitle={showGroupTitle}
                addons={addons}
                size={size}
                iconName="fa-search"
                emptyMessage={this.getEmptyMessage()}
                minSearchSymbols={this.getMinSearchSymbols()}
                focusOnItemClick={focusOnItemClick}
                loading={loading}
                items={items}
                value={this.selectedValue}
                onFocus={onFocus}
                onBlur={this.handleBlur}
                onChange={this.handleChange}
                onSearch={this.handleSearch}
            />
        );
    }
}

export default TypeaheadInput;

export const TypeaheadField: React.ComponentClass<FieldProps & TypeaheadInputProps> = formField(TypeaheadInput);
