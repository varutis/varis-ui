import * as React from 'react';
import * as cn from 'classnames';
import { Field, WrappedFieldProps } from 'redux-form';

export interface ComponentProps {
    name: string;
}

export interface FieldProps {
    className?: string;
    label?: string;
    labelWidth?: number;
    rowLayout?: boolean;
    showErrorText?: boolean;
}

export interface WrappedField<T> {
    getRenderedComponent(): T;
}

export interface FormFieldOptions {
    format?: (value: any) => any; // tslint:disable-line no-any
    normalize?: (value: any) => any; // tslint:disable-line no-any
    parse?: (value: any) => any; // tslint:disable-line no-any
}

export function formField<P extends ComponentProps>(component: React.ComponentClass<P>, options?: FormFieldOptions):
    React.ComponentClass<FieldProps & P> {
    class WrappedComponent extends React.Component<WrappedFieldProps<{}> & FieldProps & P> {
        component: React.ReactInstance | null;

        getRenderedComponent() {
            return this.component;
        }

        render() {
            const {
                input,
                meta,
                className,
                label,
                labelWidth = 4,
                rowLayout = true,
                showErrorText = false,
                ...rest
            } = this.props as (WrappedFieldProps<{}> & FieldProps); // P gives error in typescript
            const children = [];

            const hasLabel = typeof (label) !== 'undefined';
            if (hasLabel) {
                const labelProps: React.LabelHTMLAttributes<HTMLLabelElement> & React.ClassAttributes<HTMLLabelElement> = { key: 'label' };
                if (rowLayout) {
                    labelProps.className = `col-sm-${labelWidth} col-form-label`;
                }

                children.push(React.createElement('label', labelProps, label));
            }

            const inputColumn = [];
            const isInvalid = meta.error && (meta.dirty || meta.touched || meta.submitFailed);
            inputColumn.push(React.createElement(component as any, { // tslint:disable-line no-any
                key: 'input',
                ref: x => this.component = x,
                ...input,
                ...rest,
                form: meta.form,
                className: isInvalid ? `${className || ''} is-invalid` : className
            }));

            if (showErrorText && isInvalid) {
                inputColumn.push(React.createElement('div', { key: 'error', className: 'invalid-feedback' }, meta.error));
            }

            if (rowLayout && hasLabel) {
                children.push(React.createElement('div', { key: 'input', className: `col-sm-${12 - labelWidth}` }, inputColumn));
            } else {
                children.push(...inputColumn);
            }

            const containerProps = {
                className: cn('form-group', {
                    'row': rowLayout && hasLabel,
                    'has-error': !!isInvalid
                })
            };

            return React.createElement('div', containerProps, children);
        }
    }

    return class WrappedField extends React.Component<FieldProps & P> {
        field: Field | null;

        getRenderedComponent() {
            const wrappedComponent = this.field && this.field.getRenderedComponent() as any; // tslint:disable-line no-any
            return wrappedComponent && wrappedComponent.getRenderedComponent();
        }

        render() {
            return React.createElement(Field, {
                ...(this.props as object),
                ref: x => this.field = x,
                withRef: true,
                format: options && options.format,
                normalize: options && options.normalize,
                parse: options && options.parse,
                component: WrappedComponent
            });
        }
    };
}

export default formField;
