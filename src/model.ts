export interface Option<T extends (number | string) = number> {
    id: T;
    name: string;
    code?: string | null;
    type?: string | null;
    group?: string | null;
    description?: string | null;
}

export interface OptionWithDescription<T extends (number | string)> extends Option<T> {
    description?: string | null;
}

export interface ValueEvent {
    (value: any): void; // tslint:disable-line no-any
}

export type BSColor = 'primary' | 'secondary' | 'success' | 'info' | 'warning' | 'danger';

export type BSSize = 'sm' | 'lg';
