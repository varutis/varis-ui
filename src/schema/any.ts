// tslint:disable no-any
import { BaseSchema, isAbsent } from './base';

export class AnySchema extends BaseSchema<any> {
    constructor() {
        super();
        this.transform(v => v);
        this.addSyncRule(v => isAbsent(v) || this.isOfType(v) ? undefined : this.formatError('Įvesta informacija neatitinka jokio DataType tipo'));
    }

    protected isOfType(value: any): value is any {
        return true;
    }

}
