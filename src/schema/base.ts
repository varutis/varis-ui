// tslint:disable no-any

export type ValidationContext = { parent: ValidationContext | undefined, container: any, key: string | number | undefined };
export type ValidationError = undefined | string | { [P: string]: ValidationError } | { [P: number]: ValidationError };

export type Transformation = (currentValue: Readonly<any>, originalValue: Readonly<any>) => any;
export type SyncRule<T> = (value: T | null | undefined, context: ValidationContext) => ValidationError;

export const isAbsent = (v: any): v is null | undefined => v === null || v === undefined;

export abstract class BaseSchema<T> {
    private defaultLabel: string;
    private defaultValue: T | null | undefined;
    private transformations: Transformation[];
    private syncRules: SyncRule<T>[];

    constructor() {
        this.defaultValue = undefined;
        this.transformations = [];
        this.syncRules = [];
    }

    protected addSyncRule(rule: SyncRule<T>) {
        this.syncRules.push(rule);
    }

    protected abstract isOfType(value: any): value is T;

    protected formatError(message: string): string {
        return this.defaultLabel ? `${this.defaultLabel}: ${message.charAt(0).toLowerCase() + message.slice(1)}` : message;
    }

    add(isValid: (value: T, context: ValidationContext) => boolean, message: string) {
        this.addSyncRule((v: T | null | undefined, context: ValidationContext) => isAbsent(v) || isValid(v, context)
            ? undefined : this.formatError(message));
        return this;
    }

    addRule(isValid: (value: T | null | undefined, context: ValidationContext) => boolean, message: string) {
        this.addSyncRule((v: T | null | undefined, context: ValidationContext) => isValid(v, context) 
            ? undefined : this.formatError(message));
        return this;
    }

    label(value: string) {
        this.defaultLabel = value;
        return this;
    }

    convert(value: any): T | null | undefined {
        if (isAbsent(value)) {
            return this.defaultValue !== undefined ? this.defaultValue : value;
        }

        return this.transformations.reduce((currentValue, fn) => fn.call(this, currentValue, value), value);
    }

    default(value: T | null | undefined) {
        this.defaultValue = value;
        return this;
    }

    transform(transformation: Transformation) {
        this.transformations.push(transformation);
        return this;
    }

    validate(value: T | null | undefined, context?: ValidationContext): ValidationError {
        if (typeof context === 'undefined') {
            context = { parent: undefined, container: value, key: undefined };
        }

        const errors = this.syncRules.reduce((error, rule) => error || rule.call(this, value, context), undefined as ValidationError);
        // console.warn('VALIDATION', value, errors); // Leave this line commented for debugging purpose
        return errors;
    }
}
