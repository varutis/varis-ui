import { ListSchema } from './list';
import { MomentSchema } from './moment';
import { NumberSchema } from './number';
import { ObjectSchema } from './object';
import { StringSchema } from './string';
import { AnySchema } from './any';

export { ValidationError } from './base';
export { reduxSchemaForm } from './reduxForm';

export {
    ListSchema,
    MomentSchema,
    NumberSchema,
    ObjectSchema,
    StringSchema,
    AnySchema
};

export const schema = {
    list: <T>() => new ListSchema<T>(),
    moment: () => new MomentSchema(),
    number: () => new NumberSchema(),
    object: <T extends object>() => new ObjectSchema<T>(),
    string: <T extends string>() => new StringSchema<T>(),
    any: () => new AnySchema(),
};

export default schema;
