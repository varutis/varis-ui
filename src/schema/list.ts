// tslint:disable no-any
import { BaseSchema, isAbsent, ValidationContext } from './base';

const createInvalidEmptyValue = () => {
    const result: any[] = [];
    result['_isInvalid'] = true; // tslint:disable-line no-string-literal
    return result;
};

const createListError = (message: string) => {
    const error: any[] = [];
    error['_error'] = message; // tslint:disable-line no-string-literal
    return error;
};

export class ListSchema<T> extends BaseSchema<T[]> {
    private itemSchema: BaseSchema<T>;

    constructor() {
        super();
        this.transform(v => isAbsent(v) || this.isOfType(v) ? v : createInvalidEmptyValue());
        this.addSyncRule(v => isAbsent(v) || this.isOfType(v) ? undefined : createListError(this.formatError('Reikšmė turi būti sąrašas')));
    }

    protected isOfType(value: any): value is T[] {
        return value instanceof Array && !value['_isInvalid']; // tslint:disable-line no-string-literal
    }

    required() {
        this.addSyncRule(v => !isAbsent(v) ? undefined : createListError(this.formatError('Reikšmė yra privaloma')));
        return this;
    }

    min(minSize: number) {
        this.addSyncRule(
            v => isAbsent(v) || v.length >= minSize ?
                undefined : createListError(this.formatError(`Sąraše turi būti ne mažiau kaip ${minSize} įrašas(-ai)`)));
        return this;
    }

    max(maxSize: number) {
        this.addSyncRule(
            v => isAbsent(v) || v.length <= maxSize ?
                undefined : createListError(this.formatError(`Sąraše turi būti ne daugiau kaip ${maxSize} įrašai(-ų)`)));
        return this;
    }

    of(itemSchema: BaseSchema<T>) {
        this.itemSchema = itemSchema;

        this.transform(v => {
            if (!this.isOfType(v)) {
                return v;
            }

            return v.map(item => this.itemSchema.convert(item));
        });

        this.addSyncRule((v, c) => v!.reduce((errors, item, index) => {
            const context: ValidationContext = { parent: c, container: v, key: index };
            const error = this.itemSchema.validate(item, context);
            if (error) {
                errors = errors || [];
                errors[index] = error;
            }

            return errors;
        }, undefined as any));

        return this;
    }
}
