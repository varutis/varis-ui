// tslint:disable no-any
import * as moment from 'moment';
import { Moment } from 'moment';
import { BaseSchema, isAbsent } from './base';

export class MomentSchema extends BaseSchema<Moment> {
    constructor() {
        super();
        this.transform(v =>
            moment.isMoment(v) && !(v instanceof moment) ? moment(v) : (typeof(v) === 'string' && (v as string).length === 0 ? null : v)
        );
        this.addSyncRule(v => isAbsent(v) || this.isOfType(v) ? undefined : this.formatError('Reikšmė turi būti data'));
    }

    protected isOfType(value: any): value is Moment {
        return moment.isMoment(value) && value.isValid() && !isNaN(value.toDate().getTime());
    }

    required() {
        this.addSyncRule(v => !isAbsent(v) ? undefined : this.formatError('Reikšmė yra privaloma'));
        return this;
    }
}
