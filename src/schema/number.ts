// tslint:disable no-any
import { BaseSchema, isAbsent } from './base';

const parse = (value: string): number => {
    if (value.indexOf(',') >= 0) {
        value = value.replace(',', '.');
    }

    const num = parseFloat(value);

    return !isNaN(num) && isFinite(value as any) ? num : NaN;
};

function decimalAdjust(value: number, exp: number): number {
    if (exp === 0) {
        return Math.round(value);
    }

    if (value < 0) {
        return -decimalAdjust(-value, exp);
    }

    const shiftedValue = value.toString().split('e');
    value = Math.round(+(shiftedValue[0] + 'e' + (shiftedValue[1] ? (+shiftedValue[1] + exp) : exp)));

    const unshiftedValue = value.toString().split('e');
    return +(unshiftedValue[0] + 'e' + (unshiftedValue[1] ? (+unshiftedValue[1] - exp) : -exp));
}

function getDecimalCount(value: number | null | undefined) {
    if (!isAbsent(value)) {
        var splitValues = value.toString().split('.');
        return splitValues.length === 1 ? 0 : splitValues[1].length;
    } else {
        return 0;
    }
}

export class NumberSchema extends BaseSchema<number> {
    constructor() {
        super();
        this.transform(v => isAbsent(v) || this.isOfType(v) ? v : typeof (v) !== 'string' ? NaN : (v as string).length === 0 ? null : parse(v));
        this.addSyncRule(v => isAbsent(v) || this.isOfType(v) ? undefined : this.formatError('Reikšmė turi būti skaičius'));
    }

    protected isOfType(value: any): value is number {
        return typeof (value) === 'number' && !isNaN(value);
    }

    integer() {
        this.addSyncRule(v => isAbsent(v) || v === (v | 0) ? // tslint:disable-line no-bitwise
            undefined :
            this.formatError('Reikšmė turi būti sveikasis skaičius'));
        return this;
    }

    maxFractionalDigits(value: number) {
        const delta = 10 ** value;
        this.addSyncRule(v => {
            let decExp = getDecimalCount(v);
            return isAbsent(v) || (decimalAdjust(v * delta, decExp) === (decimalAdjust(v * delta, decExp) | 0)) ? // tslint:disable-line no-bitwise
                undefined :
                this.formatError(`Reikšmė negali turėti daugiau kaip ${value} skaičių po kablelio`);
            }
        );
        return this;
    }

    min(minValue: number) {
        this.addSyncRule(v => isAbsent(v) || v >= minValue ? undefined : this.formatError(`Reikšmė turi būti ne mažesnė už ${minValue}`));
        return this;
    }

    max(maxValue: number) {
        this.addSyncRule(v => isAbsent(v) || v <= maxValue ? undefined : this.formatError(`Reikšmė turi būti ne didesnė už ${maxValue}`));
        return this;
    }

    round(precision: number) {
        this.transform(v => isAbsent(v) || !this.isOfType(v) ? v : decimalAdjust(v, precision));
        return this;
    }

    optionMustExist() {
        this.addSyncRule(v => {
            return (!isAbsent(v) && this.isOfType(v) && v.toString() === '-999999999') ?
            this.formatError('Reikšmė neegzistuoja sąraše') :
            undefined;
        });
        return this;
    }

    required() {
        this.addSyncRule(v => {
            return !isAbsent(v) ? undefined : this.formatError('Reikšmė yra privaloma');
        });
        return this;
    }
}
