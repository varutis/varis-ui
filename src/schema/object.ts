// tslint:disable no-any
import { BaseSchema, isAbsent, ValidationError, ValidationContext } from './base';

export type ObjectShape<T extends object> = {
    [P in keyof T]?: BaseSchema<T[P]>;
};

export class ObjectSchema<T extends object> extends BaseSchema<T> {
    private shape: ObjectShape<T>;

    constructor() {
        super();
        this.transform(v => isAbsent(v) || this.isOfType(v) ? v : 'INVALID');
        this.addSyncRule(v => isAbsent(v) || this.isOfType(v) ? undefined : 'Reikšmė turi būti objektas');
    }

    protected isOfType(value: any): value is T {
        return Object.prototype.toString.call(value) === '[object Object]';
    }

    of(shape: ObjectShape<T>) {
        this.shape = shape;

        this.transform(v => {
            if (!this.isOfType(v)) {
                return v;
            }

            return Object.keys(this.shape).reduce((value, key) => {
                value[key] = this.shape[key as any]!.convert(value[key]);
                return value;
            }, Object.assign({}, v));
        });

        this.addSyncRule((v, c) => Object.keys(this.shape).reduce((errors, key) => {
            const context: ValidationContext = { parent: c, container: v, key };
            const error = this.shape[key]!.validate(v![key], context);
            if (error) {
                errors = errors || {};
                errors[key] = error;
            }

            return errors;
        }, undefined as ValidationError));

        return this;
    }

    required() {
        this.addSyncRule(v => !isAbsent(v) ? undefined : this.formatError('Reikšmė yra privaloma'));
        return this;
    }
}
