// tslint:disable no-any
import * as React from 'react';
import { reduxForm, DataShape, Config, FormProps } from 'redux-form';
import { ObjectSchema } from './object';

export function reduxSchemaForm<FormData extends DataShape, P, S>(schema: ObjectSchema<FormData>, config: Config<FormData, P, S>) {
    const schemaConfig: Config<FormData, P, S> = {
        ...config,
        validate: (values) => schema.validate(schema.convert(values)) as any
    };

    const createHandler = (handler: any) => (values: any, dispatch: any, props: any) => handler(schema.convert(values), dispatch, props);

    const form = reduxForm(schemaConfig);
    return function <T>(component: T): T {
        class SchemaWrapper extends React.Component<P & FormProps<FormData, P, S>> {
            handleSubmit = (handlerOrEvent: any) => {
                const originalHandleSubmit: any = this.props.handleSubmit;
                if (typeof (handlerOrEvent) === 'function') {
                    return originalHandleSubmit(createHandler(handlerOrEvent));
                }

                const onSubmit = (this.props as any).onSubmit;
                if (typeof (onSubmit) !== 'function') {
                    return originalHandleSubmit(handlerOrEvent);
                }

                return originalHandleSubmit(createHandler(onSubmit))(handlerOrEvent);
            }

            render() {
                return React.createElement(component as any, Object.assign({}, this.props, {
                    handleSubmit: this.handleSubmit
                }));
            }
        }

        return form(SchemaWrapper) as any;
    };
}
