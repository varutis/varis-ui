// tslint:disable no-any
import { BaseSchema, isAbsent } from './base';

export class StringSchema<T extends string> extends BaseSchema<T> {
    constructor() {
        super();
        this.transform(v => !isAbsent(v) && v.toString ? v.toString() : v);
        this.addSyncRule(v => isAbsent(v) || this.isOfType(v) ? undefined : 'Reikšmė turi būti tekstas');
    }

    protected isOfType(value: any): value is T {
        return typeof (value) === 'string';
    }

    maxLength(length: number) {
        this.addSyncRule(v => isAbsent(v) || v!.length <= length ?
            undefined :
            this.formatError(`Reikšmė turi turėti ne daugiau kaip ${length} simbolių`));
        return this;
    }

    optionMustExist() {
        this.addSyncRule(v => {
            return (!isAbsent(v) && this.isOfType(v) && v.toString() === '-999999999') ?
            this.formatError('Reikšmė neegzistuoja sąraše') :
            undefined;
        });
        return this;
    }

    required() {
        this.addSyncRule(v => !isAbsent(v) && v!.length > 0 ? undefined : this.formatError('Reikšmė yra privaloma'));
        return this;
    }
}
