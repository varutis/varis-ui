export const nameToId = (name: string, form?: string) => `${form ? form + '_' : ''}${name.replace(/[\[\]\.]/g, '_')}`;
