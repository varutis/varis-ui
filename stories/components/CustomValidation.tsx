import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { DateTimeField /*, DropDownField, MultiDownshiftField, CheckBoxField, NumberField, TextField */ } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';
import * as enums from './enums';
import { Option } from '../../src/model';
import moment from 'moment';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

export const isNumber = (value: number | string) => typeof value === 'number' || /^[-+]?([0-9]+|[\.,][0-9]+|[0-9]+[\.,][0-9]+)$/.test(value);

interface DataModel {
    fields: FieldListItemModel[];
}

interface FieldListItemModel {
    id: number;
    secondId: number | null;
    label: string | null;
    secondLabel: string | null;
    value: any | null; // tslint:disable-line:no-any
    secondValue: any | null; // tslint:disable-line:no-any
    name: string | null;
    secondName: string | null;
    comment: string | null;
    secondComment: string | null;
    dataType: enums.ParameterUiType;
}

const formSchema = schema.object<DataModel>().of({
    fields: schema.list<FieldListItemModel>().of(schema.object<FieldListItemModel>().of({
        value: schema.any().add((x, c) => {
            const cont: FieldListItemModel = c.container;
            console.warn('Input:', x, 'Container:', cont);
            /* implying if check for component */ // DateRange
            /*
            if (true) {
                let returnValue: { value: boolean, secondValue: boolean } = { value: false, secondValue: false };
                if (cont.value != null) {
                    returnValue.value = moment.isMoment(cont.value) && cont.value.isValid() && !isNaN(cont.value.toDate().getTime());
                }
                if (cont.secondValue != null) {
                    returnValue.secondValue = moment.isMoment(cont.secondValue) && cont.secondValue.isValid() &&
                        !isNaN(cont.secondValue.toDate().getTime()); // tslint:disable-line:max-line-length
                }
                if (returnValue.value && returnValue.secondValue === true) {
                    return true;
                }
                return false;
            }*/
            /* implying if check for component */ // DropDownSingle
            /*
            if (true) {
                return isNumber(x);
            }*/
            /* implying if check for component */ // Text
            /*
            if (true) {
                return true;
            }*/
            /* implying if check for component */ // LookupMultiple
            /*
            if (true) {
                if (Array.isArray(x) && x instanceof Array) {
                    return Object.prototype.toString.call(x) === '[object Array]';
                }
                return false;
            }*/
            /* implying if check for component */ // Date
            /*
            if (true) {
                return moment.isMoment(cont.value) && cont.value.isValid() && !isNaN(cont.value.toDate().getTime());
            }
            */
            /* implying if check for component */ // DateTime
            /*
            if (true) {
                return moment.isMoment(cont.value) && cont.value.isValid() && !isNaN(cont.value.toDate().getTime());
            }
            */
            /* implying if check for component */ // Time
            /*
            if (true) {
                return moment.isMoment(cont.value) && cont.value.isValid() && !isNaN(cont.value.toDate().getTime());
            }*/
            /* implying if check for component */ // DateTimeRange
            /*
            if (true) {
                let returnValue: { value: boolean, secondValue: boolean } = { value: false, secondValue: false };
                if (cont.value != null) {
                    returnValue.value = moment.isMoment(cont.value) && cont.value.isValid() && !isNaN(cont.value.toDate().getTime());
                }
                if (cont.secondValue != null) {
                    returnValue.secondValue = moment.isMoment(cont.secondValue) && cont.secondValue.isValid() &&
                        !isNaN(cont.secondValue.toDate().getTime());
                }
                if (returnValue.value && returnValue.secondValue === true) {
                    return true;
                }
                return false;
            }*/
            /* implying if check for component */ // TimeRange
            if (true) {
                let returnValue: { value: boolean, secondValue: boolean } = { value: false, secondValue: false };
                if (cont.value != null) {
                    returnValue.value = moment.isMoment(cont.value) && cont.value.isValid() && !isNaN(cont.value.toDate().getTime());
                }
                if (cont.secondValue != null) {
                    returnValue.secondValue = moment.isMoment(cont.secondValue) && cont.secondValue.isValid() &&
                        !isNaN(cont.secondValue.toDate().getTime());
                }
                if (returnValue.value && returnValue.secondValue === true) {
                    return true;
                }
                return false;
            }
            /* implying if check for component */ // Checkbox
            /*
            if (true) {
                return typeof(x) === 'boolean' ? true : false;
            }*/
            /* implying if check for component */ // Number
            /*
            if (true) {
                return isNumber(x);
            }*/
            /* implying if check for component */ // NumberRange
            /*
            if (true) {
                let returnValue: { value: boolean, secondValue: boolean } = { value: false, secondValue: false };
                if (cont.value != null) {
                    returnValue.value = isNumber(cont.value);
                }
                if (cont.secondValue != null) {
                    returnValue.secondValue = isNumber(cont.secondValue);
                }
                if (returnValue.value && returnValue.secondValue === true) {
                    return true;
                }
                return false;
            }*/
            // return true; // or false (false - error in form; true - no errors in form)
        }, 'Error in form message')
    }))
});

@reduxSchemaForm(formSchema, {
    form: 'CustomValidationFieldStory',
    initialValues: {
        fields: []
    }
})
class CustomValidationFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    handleOnSearch = async (text: string, data: Option<number>[]) => {
        console.warn('handle', text);
        return await data;
    }

    render() {
        const { handleSubmit } = this.props;
        /*
        const options = [
            { id: 1, name: 'Pirmas', code: null, type: null, group: null },
            { id: 2, name: 'Antras', code: null, type: null, group: null },
            { id: 'a', name: 'Trecias', code: null, type: null, group: null }
        ];*/
        /*
        const options2: Option[] = [
            { id: 1, name: 'Pirmas', code: null, type: null, group: null },
            { id: 2, name: 'Antras', code: null, type: null, group: null },
            { id: 3, name: 'Trecias', code: null, type: null, group: null }
        ];
        */

        const test = {
            fields: [/*{
                comment: 'DataRange',
                id: 1,
                label: 'Nuo',
                name: '@EtapoDataNuo',
                secondComment: null,
                secondId: 2,
                secondLabel: 'iki',
                secondName: '@EtapoDataIki',
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.DateRange
            },
            {
                comment: 'DropDownSingle',
                id: 12,
                label: 'DropDownSingle',
                name: '@DropDownSingle',
                secondComment: null,
                secondId: null,
                secondLabel: null,
                secondName: null,
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.DropDownSingle
            },
            {
                comment: 'LookupMultiple',
                id: 13,
                label: 'LookupMultiple',
                name: '@LookupMultiple',
                secondComment: null,
                secondId: null,
                secondLabel: null,
                secondName: null,
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.LookupMultiple
            },
            {
                comment: 'Date',
                id: 13,
                label: 'Date',
                name: '@Date',
                secondComment: null,
                secondId: null,
                secondLabel: null,
                secondName: null,
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.Date
            },
            {
                comment: 'DateTime',
                id: 11,
                label: 'DateTime',
                name: '@DateTime',
                secondComment: null,
                secondId: null,
                secondLabel: null,
                secondName: null,
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.DateTime
            },
            {
                comment: 'Time',
                id: 18,
                label: 'Time',
                name: '@Time',
                secondComment: null,
                secondId: null,
                secondLabel: null,
                secondName: null,
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.Time
            },
            {
                comment: 'DateTimeRange',
                id: 18,
                label: 'DateTimeRange',
                name: '@DateTimeRange',
                secondComment: 'DateTimeRange',
                secondId: 19,
                secondLabel: 'iki',
                secondName: '@DateTimeRangeIki',
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.DateTimeRange
            },
            */
                {
                    comment: 'TimeRange',
                    id: 18,
                    label: 'TimeRange',
                    name: '@TimeRange',
                    secondComment: 'TimeRange',
                    secondId: 19,
                    secondLabel: 'iki',
                    secondName: '@TimeRangeIki',
                    secondValue: null,
                    value: null,
                    dataType: enums.ParameterUiType.TimeRange
                } /*,
            {
                comment: 'Checkbox',
                id: 19,
                label: 'Checkbox',
                name: '@Checkbox',
                secondComment: null,
                secondId: null,
                secondLabel:  null,
                secondName: null,
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.Checkbox
            },
            {
                comment: 'Number',
                id: 20,
                label: 'Number',
                name: '@Number',
                secondComment: null,
                secondId: null,
                secondLabel:  null,
                secondName: null,
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.Number
            },
            {
                comment: 'NumberRange',
                id: 21,
                label: 'NumberRange',
                name: '@NumberRange',
                secondComment: 'NumberRange',
                secondId: 22,
                secondLabel: 'NumberRange',
                secondName: '@NumberRangeIki',
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.NumberRange
            },
            {
                comment: 'Text',
                id: 23,
                label: 'Text',
                name: '@Text',
                secondComment: null,
                secondId: null,
                secondLabel: null,
                secondName: null,
                secondValue: null,
                value: null,
                dataType: enums.ParameterUiType.Text
            }*/]
        } as DataModel;

        return (
            <fieldset>
                <legend>CustomValidation</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    {test.fields.map((field: FieldListItemModel, index: number) =>
                        (<React.Fragment key={index}>
                            {/* <DateTimeField
                                name={`fields[${index}].value`}
                                labelWidth={6}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                showDate={true}
                                showErrorText={true}
                            />
                            <DateTimeField
                                name={`fields[${index}].secondValue`}
                                labelWidth={2}
                                description={field.secondComment ? field.secondComment : ''}
                                label={field.secondLabel ? field.secondLabel : ''}
                                showDate={true}
                                showErrorText={true}
                            />

                            <DropDownField
                                name={`fields[${index}].value`}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                options={options}
                                showErrorText={true}
                            />

                            <MultiDownshiftField
                                name={`fields[${index}].value`}
                                // description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                onSearch={(text: string) => this.handleOnSearch(text, options2)}
                                showErrorText={true}
                            />

                            <DateTimeField
                                name={`fields[${index}].value`}
                                labelWidth={6}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                showDate={true}
                                showTime={false}
                                showErrorText={true}
                            />

                            <DateTimeField
                                name={`fields[${index}].value`}
                                labelWidth={6}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                showDate={true}
                                showTime={true}
                            />
                            <DateTimeField
                                name={`fields[${index}].value`}
                                labelWidth={6}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                showDate={false}
                                showTime={true}
                            />

                            <DateTimeField
                                name={`fields[${index}].value`}
                                labelWidth={6}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                showDate={true}
                                showTime={true}
                            />
                            <DateTimeField
                                name={`fields[${index}].secondValue`}
                                labelWidth={2}
                                description={field.secondComment ? field.secondComment : ''}
                                label={field.secondLabel ? field.secondLabel : ''}
                                showDate={true}
                                showTime={true}
                            /> */}

                            <DateTimeField
                                name={`fields[${index}].value`}
                                labelWidth={6}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                showDate={false}
                                showTime={true}
                            />
                            <DateTimeField
                                name={`fields[${index}].secondValue`}
                                labelWidth={2}
                                description={field.secondComment ? field.secondComment : ''}
                                label={field.secondLabel ? field.secondLabel : ''}
                                showDate={false}
                                showTime={true}
                            />
                            {/*

                            <CheckBoxField
                                name={`fields[${index}].value`}
                                description={field.comment ? field.comment : ''}
                                text={field.label ? field.label : ''}
                            />

                            <NumberField
                                name={`fields[${index}].value`}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                showErrorText={true}
                            />

                            <NumberField
                                name={`fields[${index}].value`}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                                labelWidth={6}
                            />
                            <NumberField
                                name={`fields[${index}].secondValue`}
                                description={field.secondComment ? field.secondComment : ''}
                                label={field.secondLabel ? field.secondLabel : ''}
                            />

                            <TextField
                                name={`fields[${index}].value`}
                                description={field.comment ? field.comment : ''}
                                label={field.label ? field.label : ''}
                            /> */}
                        </React.Fragment>)
                    )}
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const customValidationFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <CustomValidationFieldStoryRenderer />
        </Provider>
    );
};