import { RenderFunction } from '@storybook/react';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { reducer as formReducer } from 'redux-form';
import { SortableList, SortableListItem } from '../../src/components';
import { SortEnd, arrayMove } from 'react-sortable-hoc';

export interface SortableStoryState {
    items: string[];
}

class SortableStoryRenderer extends React.Component<{}, SortableStoryState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            items: ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6']
        };
    }

    handleOnSortEnd = ({ oldIndex, newIndex }: SortEnd) => {
        this.setState({
            items: arrayMove(this.state.items, oldIndex, newIndex),
        });
    }

    render() {
        const { items } = this.state;
        return (
            <fieldset>
                <legend>Sortable</legend>
                <SortableList onSortEnd={this.handleOnSortEnd}>
                    {items.map((value, index) => (
                        <SortableListItem key={`item-${index}`} index={index}>
                            <div className="card text-white bg-info">
                                <div className="card-body">
                                    <div className="form-row">
                                        <div className="col-6">
                                            {value}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </SortableListItem>
                    ))}
                </SortableList>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }));
export const sortableFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <SortableStoryRenderer />
        </Provider>
    );
};
