import { storiesOf } from '@storybook/react';
import { sortableFieldStory } from './SortableFieldStories';
import { customValidationFieldStory } from './CustomValidation';

storiesOf('Components', module)
    .add('Sortable', sortableFieldStory)
    .add('Customvalidation', customValidationFieldStory);
