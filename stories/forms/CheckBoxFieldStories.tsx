import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { CheckBoxField } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value: boolean | null | undefined;
}

const formSchema = schema.object<DataModel>().of({
});

@reduxSchemaForm(formSchema, { form: 'CheckBoxFieldStory' })
class CheckBoxFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>CheckBoxField</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <CheckBoxField name="value" text="CheckBox text" description="CheckBox field description" />
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const checkBoxFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <CheckBoxFieldStoryRenderer />
        </Provider>
    );
};
