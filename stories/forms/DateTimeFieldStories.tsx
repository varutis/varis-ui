import * as moment from 'moment';
import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { DateTimeField } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value: moment.Moment | null | undefined;
    value2: moment.Moment | null | undefined;
    value3: moment.Moment | null | undefined;
}

const formSchema = schema.object<DataModel>().of({
    value: schema.moment().required(),
    value2: schema.moment().required(),
    value3: schema.moment().required()
});

@reduxSchemaForm(formSchema, { form: 'DateTimeFieldStory' })
class DateTimeFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>DateTimeField</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <DateTimeField
                        name="value"
                        showDate={false}
                        showTime={true}
                        rowLayout={true}
                        showToggleButton={false}
                        placeholder="__:__"
                    />
                    <DateTimeField
                        name="value2"
                        placeholder="DateTimeField"
                        description="DateTime field description"
                        showErrorText={true}
                        showDate={true}
                        showTime={true}
                        showToggleButton={false}
                    />
                    <DateTimeField
                        name="value3"
                        placeholder="DateTimeField"
                        description="DateTime field description"
                        showErrorText={true}
                    />
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const dateTimeFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <DateTimeFieldStoryRenderer />
        </Provider>
    );
};
