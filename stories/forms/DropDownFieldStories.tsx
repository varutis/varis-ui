import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { DropDownField } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value: number | null | undefined;
    value2: string | null | undefined;
}

const formSchema = schema.object<DataModel>().of({
    value: schema.number().required().optionMustExist(),
    value2: schema.string().required().optionMustExist(),
});

const options = [
    { id: 1, name: 'Pirmas', code: 'code-1', type: null, group: null },
    { id: 2, name: 'Antras', code: 'code-2', type: null, group: null }
];

const options2 = [
    { id: '1', name: 'Pirmas', code: 'Z00.1', type: null, group: null },
    { id: '2', name: 'Antras', code: 'Z00.5', type: null, group: null }
];

@reduxSchemaForm(formSchema, {
    form: 'DropDownFieldStory',
    initialValues: {
        value: 2,
        value2: '3'
    }
})
class DropDownFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>DropDownField</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <DropDownField
                        name="value"
                        options={options}
                        placeholder="DropDownField Number schema"
                        description="DropDown field number description"
                        showErrorText={true}
                    />
                    <DropDownField
                        name="value2"
                        options={options2}
                        placeholder="DropDownField String schema"
                        description="DropDown field string description"
                        showErrorText={true}
                        showCodeWithName={true}
                    />
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const dropDownFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <DropDownFieldStoryRenderer />
        </Provider>
    );
};
