import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { MultiDownshiftField } from '../../src/forms/Multi/MultiDownshift';
import { schema, reduxSchemaForm } from '../../src/schema';
import { DropDownItem } from '../../src/forms/DropDownBase';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value1: DropDownItem[] | null | undefined;
}

const formSchema = schema.object<DataModel>().of({
});

type OwnProps = FormProps<DataModel, {}, {}>;

const handleOnTypeaheadSearch = (query: string) => {
    let test = fetch(`https://api.github.com/search/users?q=${query}`)
        .then(resp => resp.json())
        .then(json => {
            return json.items ?
                json.items.map((x: any, i: number) => ({ id: i, name: x.login })) : // tslint:disable-line no-any
                [] as DropDownItem[];
        }
        );

    return test;
};

@reduxSchemaForm(formSchema, { form: 'MultipleFieldStory' })
class MultipleFieldStoryRenderer extends React.Component<OwnProps> {

    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>MultipleField</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <MultiDownshiftField
                        name="value1"
                        onSearch={handleOnTypeaheadSearch}
                    />
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const multipleFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <MultipleFieldStoryRenderer />
        </Provider>
    );
};
