import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { NumberField } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value: number | null | undefined;
    priceValue: number | null;
}

const formSchema = schema.object<DataModel>().of({
    value: schema.number().required(),
    priceValue: schema.number().min(0).required().maxFractionalDigits(2)
});

@reduxSchemaForm(formSchema, {
    form: 'NumberFieldStory',
    initialValues: {
    }
})

class NumberFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>NumberField</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <NumberField name="value" placeholder="Number Field" description="Number field description" />
                    <NumberField
                        name="priceValue"
                        className="text-right"
                        showErrorText={true}
                        addon={
                            <span key="currency" className="input-group-append">
                                <span className="input-group-text">&euro;</span>
                            </span>
                        }
                    />
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const numberFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <NumberFieldStoryRenderer />
        </Provider>
    );
};
