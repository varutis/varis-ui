import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { RadioButtonListField } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value: string | null | undefined;
}

const formSchema = schema.object<DataModel>().of({
    value: schema.string().required()
});

const options = [
    { name: 'Radio1', id: 'radio1', code: null, type: null, group: null },
    { name: 'Radio2', id: 'radio2', code: null, type: null, group: null },
    { name: 'Radio3', id: 'radio3', code: null, type: null, group: null },
];

const optionsWithDescription = [
    { name: 'Radio1', id: 'radio1', code: null, type: null, group: null, description: 'description1' },
    { name: 'Radio2', id: 'radio2', code: null, type: null, group: null, description: 'description2' },
    { name: 'Radio3', id: 'radio3', code: null, type: null, group: null, description: 'description3' },
];

@reduxSchemaForm(formSchema, { form: 'RadioButtonListFieldStory' })
class RadioButtonListFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>RadioButtonListField</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <RadioButtonListField name="value" options={options} description="RadioButtonList field description" />
                    <hr />
                    <RadioButtonListField
                        name="value2"
                        options={optionsWithDescription}
                        description="RadioButtonList field description2"
                        inlineForm={false}
                    />
                </form>
            </fieldset >
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const radioButtonListFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <RadioButtonListFieldStoryRenderer />
        </Provider>
    );
};
