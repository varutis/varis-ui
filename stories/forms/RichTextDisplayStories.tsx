import { RenderFunction } from '@storybook/react';
import React from 'react';
import { RichTextDisplay } from '../../src/forms';

class RichTextDisplayStoryRenderer extends React.Component {
    render() {
        return (
            <fieldset>
                <legend>RichTextDisplay</legend>
                <RichTextDisplay value="Pavyzdinis <b>rich</b> <em>text</em> <i>display</i> tekstas"/>
            </fieldset>
        );
    }
}

export const richTextDisplayStory: RenderFunction = () => {
    return (
        <RichTextDisplayStoryRenderer />
    );
};
