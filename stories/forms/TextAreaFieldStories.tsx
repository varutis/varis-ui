import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { TextAreaField } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value: string | null | undefined;
}

const formSchema = schema.object<DataModel>().of({
    value: schema.string().required()
});

@reduxSchemaForm(formSchema, { form: 'TextAreaFieldStory' })
class TextAreaFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>TextAreaField</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <TextAreaField name="value" placeholder="TextAreaField" description="Textarea field description" />
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const textAreaFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <TextAreaFieldStoryRenderer />
        </Provider>
    );
};
