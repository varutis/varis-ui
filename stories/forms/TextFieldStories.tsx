import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { composeWithDevTools } from 'redux-devtools-extension';
import { TextField } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value: string | null | undefined;
}

const formSchema = schema.object<DataModel>().of({
    value: schema.string().required()
});

@reduxSchemaForm(formSchema, { form: 'TextFieldStory' })
class TextFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>TextField</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <TextField name="value" placeholder="TextField" description="Text field description" readonly={true} />
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const textFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <TextFieldStoryRenderer />
        </Provider>
    );
};
