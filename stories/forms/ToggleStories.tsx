import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { ToggleField } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value: boolean | null | undefined;
}

const formSchema = schema.object<DataModel>().of({
});

@reduxSchemaForm(formSchema, { form: 'ToggleFieldStory' })
class ToggleFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>Toggle</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <ToggleField name="value" description="toggle field description" />
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const toggleFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <ToggleFieldStoryRenderer />
        </Provider>
    );
};
