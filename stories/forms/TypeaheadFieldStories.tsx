import { RenderFunction } from '@storybook/react';
import { decorateAction } from '@storybook/addon-actions';
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import { reducer as formReducer, FormProps } from 'redux-form';
import { TypeaheadField } from '../../src/forms';
import { schema, reduxSchemaForm } from '../../src/schema';
import { Option } from '../../src/model';

const submitAction = decorateAction([args => args.slice(0, 1).map(x => JSON.stringify(x))])('SUBMIT');

interface DataModel {
    value: Option | null | undefined;
}

const formSchema = schema.object<DataModel>().of({
    value: schema.object<Option>().required()
});

const handleOnTypeaheadSearch = (query: string) => {
    let test = fetch(`https://api.github.com/search/users?q=${query}`)
        .then(resp => resp.json())
        .then(json =>
            json.items.map((x: any, i: number) => ({ id: i, name: x.login })) // tslint:disable-line no-any
        );
    return test;
};

@reduxSchemaForm(formSchema, { form: 'TypeaheadFieldStory' })
class TypeaheadFieldStoryRenderer extends React.Component<FormProps<DataModel, {}, {}>> {
    render() {
        const { handleSubmit } = this.props;
        return (
            <fieldset>
                <legend>TypeaheadField</legend>
                <form onSubmit={handleSubmit!(submitAction)}>
                    <TypeaheadField
                        name="value"
                        placeholder="TypeaheadField"
                        description="Typeahead field description"
                        onSearch={handleOnTypeaheadSearch}
                        minSearchSymbols={0}
                    />
                </form>
            </fieldset>
        );
    }
}

const store = createStore(combineReducers({ form: formReducer }), composeWithDevTools());
export const typeaheadFieldStory: RenderFunction = () => {
    return (
        <Provider store={store}>
            <TypeaheadFieldStoryRenderer />
        </Provider>
    );
};
