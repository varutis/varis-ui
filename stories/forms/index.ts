import { storiesOf } from '@storybook/react';
import { checkBoxFieldStory } from './CheckBoxFieldStories';
import { dateTimeFieldStory } from './DateTimeFieldStories';
import { dropDownFieldStory } from './DropDownFieldStories';
import { multipleFieldStory } from './MultipleFieldStories';
import { numberFieldStory } from './NumberFieldStories';
import { passwordFieldStory } from './PasswordFieldStories';
import { radioButtonListFieldStory } from './RadioButtonListFieldStories';
import { richTextDisplayStory } from './RichTextDisplayStories';
import { textFieldStory } from './TextFieldStories';
import { textAreaFieldStory } from './TextAreaFieldStories';
import { typeaheadFieldStory } from './TypeaheadFieldStories';
import { toggleFieldStory } from './ToggleStories';

storiesOf('Forms', module)
    .add('CheckBoxField', checkBoxFieldStory)
    .add('DateTimeField', dateTimeFieldStory)
    .add('DropDownField', dropDownFieldStory)
    .add('MultipleField', multipleFieldStory)
    .add('NumberField', numberFieldStory)
    .add('PasswordField', passwordFieldStory)
    .add('RadioButtonListField', radioButtonListFieldStory)
    .add('RichTextDisplay', richTextDisplayStory)
    .add('TextField', textFieldStory)
    .add('TextAreaField', textAreaFieldStory)
    .add('TypeaheadField', typeaheadFieldStory)
    .add('Toggler', toggleFieldStory);
