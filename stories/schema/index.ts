import { storiesOf } from '@storybook/react';
import { numberStory } from './number';
import { stringStory } from './string';
import { objectListStory } from './objectList';

storiesOf('Schema', module)
    .add('Number', numberStory)
    .add('String', stringStory)
    .add('ObjectList', objectListStory);
