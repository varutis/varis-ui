import React from 'react';
import { RenderFunction } from '@storybook/react';
import { schema, NumberSchema, ValidationError } from '../../src/schema';

interface SchemaRendererProps {
    title: string;
    subtitle: string;
    schema: NumberSchema;
}

interface SchemaRendererState {
    value: string | undefined;
    converted: string | number | null | undefined;
    error: ValidationError;
}

class SchemaRenderer extends React.Component<SchemaRendererProps, SchemaRendererState> {
    state = {
        value: undefined,
        converted: undefined,
        error: undefined
    };

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { schema } = this.props;
        const value = e.currentTarget.value;
        const converted = schema.convert(value);
        const error = schema.validate(converted);
        this.setState({ value, converted, error });
    }

    render() {
        const { title, subtitle } = this.props;
        return (
            <fieldset>
                <legend>{title}</legend>
                <p><code>{subtitle}</code></p>
                <div className="form-group">
                    <input className="form-control" value={this.state.value} onChange={this.handleChange} />
                </div>
                <pre>Value: {JSON.stringify(this.state.value)}</pre>
                <pre>Converted: {JSON.stringify(this.state.converted)}</pre>
                <pre>Error: {JSON.stringify(this.state.error)}</pre>
            </fieldset>
        );
    }
}

export const numberStory: RenderFunction = () => {
    return (
        <React.Fragment>
            <SchemaRenderer
                title="Number schema"
                subtitle="schema.number().required().maxFractionalDigits(2)"
                schema={schema.number().required().maxFractionalDigits(2)}
            />
            <SchemaRenderer
                title="Integer schema"
                subtitle="schema.number().required().integer()"
                schema={schema.number().required().integer()}
            />
        </React.Fragment>
    );
};
