import React from 'react';
import { RenderFunction } from '@storybook/react';
import { schema, ObjectSchema, ValidationError } from '../../src/schema';

interface SchemaRendererProps {
    title: string;
    subtitle: string;
    schema: ObjectSchema<StateModel>;
}
interface StateModel {
    id: number;
    fields: FieldListItemModel[];
}

interface FieldListItemModel {
    id: number;
    label: string | null;
    value: number | null;
}

interface SchemaRendererState {
    value: any; // tslint:disable-line no-any
    converted: StateModel | null | undefined;
    error: ValidationError;
}

class SchemaRenderer extends React.Component<SchemaRendererProps, SchemaRendererState> {
    state = {
        value: { id: 1, fields: [{ id: 1, label: 'aaa', value: 5 }] },
        converted: undefined,
        error: undefined
    };

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { schema } = this.props;
        const input = e.currentTarget.value;

        const value = {
            ...this.state.value,
            fields: [{ ...this.state.value.fields[0], value: input }]
        };

        const converted = schema.convert(value);
        const error = schema.validate(converted);
        this.setState({ value, converted, error });
    }

    render() {
        const { title, subtitle } = this.props;
        return (
            <fieldset>
                <legend>{title}</legend>
                <p><code>{subtitle}</code></p>
                <div className="form-group">
                    <input className="form-control" value={this.state.value.fields[0].value} onChange={this.handleChange} />
                </div>
                <pre>Value: {JSON.stringify(this.state.value)}</pre>
                <pre>Converted: {JSON.stringify(this.state.converted)}</pre>
                <pre>Error: {JSON.stringify(this.state.error)}</pre>
            </fieldset>
        );
    }
}

export const objectListStory: RenderFunction = () => {
    return (
        <React.Fragment>
            <SchemaRenderer
                title="Object list schema"
                subtitle="sub"
                schema={
                    schema.object<StateModel>().of({
                        fields: schema.list<FieldListItemModel>().of(schema.object<FieldListItemModel>().of({
                            value: schema.number().label('qwer').add((x, c) => x === c.container.id, 'Error mesage'),
                        }))
                    })
                }
            />
        </React.Fragment>
    );
};