import React from 'react';
import { RenderFunction } from '@storybook/react';
import { schema, StringSchema, ValidationError } from '../../src/schema';

interface SchemaRendererProps {
    title: string;
    subtitle: string;
    schema: StringSchema<string>;
}

interface SchemaRendererState {
    value: string | undefined;
    converted: string | null | undefined;
    error: ValidationError;
}

class SchemaRenderer extends React.Component<SchemaRendererProps, SchemaRendererState> {
    state = {
        value: undefined,
        converted: undefined,
        error: undefined
    };

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { schema } = this.props;
        const value = e.currentTarget.value;
        const converted = schema.convert(value);
        const error = schema.validate(converted);
        this.setState({ value, converted, error });
    }

    render() {
        const { title, subtitle } = this.props;
        return (
            <fieldset>
                <legend>{title}</legend>
                <p><code>{subtitle}</code></p>
                <div className="form-group">
                    <input className="form-control" value={this.state.value} onChange={this.handleChange} />
                </div>
                <pre>Value: {JSON.stringify(this.state.value)}</pre>
                <pre>Converted: {JSON.stringify(this.state.converted)}</pre>
                <pre>Error: {JSON.stringify(this.state.error)}</pre>
            </fieldset>
        );
    }
}

export const stringStory: RenderFunction = () => {
    return (
        <React.Fragment>
            <SchemaRenderer
                title="String schema"
                subtitle="schema.string().required().maxLength(10)"
                schema={schema.string().required().maxLength(10)}
            />
        </React.Fragment>
    );
};
